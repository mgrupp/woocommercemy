﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

using WooCommerceMy;

//http://bongourmet.tibuilder.net/upload/fotoarticulo/0000000116_172669202.jpg

namespace WooCommerceTest {
	public partial class Form1 : Form {
		MyWooCommerce MyWooCommerce;
		private List<WooCategory> categorias;
		private List<WooProductAttribute> atributos;

		private WooCustomer oCliente = new WooCustomer();
		private WooProductAttribute oWooAttribute;
		private WooProductAttributeTerm oWooAttributeTerm;

		//--------------------------------------------------------------------------

		//STRADA BIKES

		private static String Url = "https://www.strada.pe/bikes/";
		public static String key = "ck_8f59691369072b981009e0974325556cf9c1beeb";
		public static String secret = "cs_31f1c27a2950117ba25e57df8bcaf5623c716b7c";

		//FASTADELIVERY


		//private static String Url = "https://fastadelivery.com";
		//private static String key = "ck_0c8c68d79c6fdbe2d34de5a4510bcbd240309989";
		//private static String secret = "cs_ce9d90f5bb375b13bb57d644995958dbc40fd4b5";


		//private static String Url = "https://elcebillano.quezada112.com";

		//private static String key = "ck_b0babbd8f22c7039f027a0909b0169a1d7774910";
		//private static String secret = "cs_2b1c18d16863920fad36c0edca55f44c3d0f3980";


		WooProduct productoSeleccionado = null;

		public Form1() {
			InitializeComponent();

			MyWooCommerce = new MyWooCommerce( Url, key, secret );

			versionTextBox.Text = MyWooCommerce.Version;

			MostrarCategorias();
			MostrarAtributos();
		}

		public void limpiarProducto() {
			productoSeleccionado = null;

			textBoxNombreLargo.Clear();
			textBoxNombreCorto.Clear();
			textBoxURLImagen.Clear();
			richTextBoxCaracteristicas.Clear();
			textBoxPrecioDeVenta.Clear();
			pictureBoxProducto.Image = null;
			productoEnOfertaCheckBox.Checked = false;
			inicioOfertadateTimePicker.Value = DateTime.Now;
			finalOfertaDateTimePicker.Value = DateTime.Now;
			precioDeOfertaTextBox.Clear();
			manejaExistenciaCheckBox.Checked = false;
			ExistenciaTextBox.Clear();
			totalDeVentasTextBox.Clear();

			atributosDelProductoDataGridView.DataSource = null;
			terminosCheckedListBox.Items.Clear();

			variantesDataGridView.DataSource = null;

			LimpiarCategorias();
			LimpiarAtributos();
		}

		private void LimpiarCategorias() {
			while (checkedListBoxCategorias.CheckedIndices.Count > 0)
				checkedListBoxCategorias.SetItemChecked( checkedListBoxCategorias.CheckedIndices[ 0 ], false );
		}

		private void LimpiarAtributos() {
			atributorCheckedListBox.ItemCheck -= atributorCheckedListBox_ItemCheck;

			while (atributorCheckedListBox.CheckedIndices.Count > 0)
				atributorCheckedListBox.SetItemChecked( atributorCheckedListBox.CheckedIndices[ 0 ], false );

			atributorCheckedListBox.ItemCheck += atributorCheckedListBox_ItemCheck;
		}

		public void limpiarAtributo() {
			oWooAttribute = null;
			NombreDelAtributo_TextBox.Text = "";
			terminosDataGridView.DataSource = null;

			limpiarTermino();
		}

		public void limpiarTermino() {
			oWooAttributeTerm = null;
			nombreDelTerminoTextBox.Text = "";

		}

		private void MostrarCategorias() {
			categorias = MyWooCommerce.LeerTodasLasCategorias();

			checkedListBoxCategorias.Items.Clear();
			foreach (WooCategory categoria in categorias) {
				checkedListBoxCategorias.Items.Add( categoria, false );
			}
		}

		private void MostrarAtributos() {
			atributos = MyWooCommerce.LeerTodosLosAtributos();

			atributorCheckedListBox.Items.Clear();
			foreach (WooProductAttribute atributo in atributos) {
				atributorCheckedListBox.Items.Add( atributo, false );
			}
		}

		private void MostrarTerminos( int WooIdProductAttribute ) {
			List<WooProductAttributeTerm> WooProductAttributeTerm = MyWooCommerce.LeerTodosLosTerminosDelAtributo( WooIdProductAttribute );

			terminosCheckedListBox.Items.Clear();
			foreach (WooProductAttributeTerm oWooProductAttributeTerm in WooProductAttributeTerm) {
				terminosCheckedListBox.Items.Add( oWooProductAttributeTerm, false );
			}
		}

		private void MarcarTerminos( WooProductAttributeLine oWooProductAttributeLine, List<string> terminos ) {
			terminosCheckedListBox.Tag = oWooProductAttributeLine;

			terminosCheckedListBox.ItemCheck -= terminosCheckedListBox_ItemCheck;

			for (int i = 0; i < terminosCheckedListBox.Items.Count; i++) {
				terminosCheckedListBox.SetItemChecked( i, false );
				if (terminos.Contains( ((WooProductAttributeTerm)terminosCheckedListBox.Items[ i ]).WOONOMBREDELTERMINO ))
					terminosCheckedListBox.SetItemChecked( i, true );
			}
			terminosCheckedListBox.ItemCheck += terminosCheckedListBox_ItemCheck;
		}

		private void MarcarCategorias() {
			List<WooProductCategory> categoriasProducto = productoSeleccionado.oCategoriasProducto;

			LimpiarCategorias();

			foreach (WooProductCategory categoriaProducto in categoriasProducto) {
				for (int i = 0; i < checkedListBoxCategorias.Items.Count; i++) {
					WooCategory categoria = (WooCategory)checkedListBoxCategorias.Items[ i ];
					if (categoria.WOOIDCATEGORIA == categoriaProducto.WOOIDCATEGORIAPRODUCTO) {
						checkedListBoxCategorias.SetItemChecked( i, true );
					}
				}
			}
		}

		private void MarcarAtributos() {

			LimpiarAtributos();

			atributorCheckedListBox.ItemCheck -= atributorCheckedListBox_ItemCheck;

			foreach (WooProductAttributeLine oWooProductAttributeLine in productoSeleccionado.oWooProductAttributeLines) {
				for (int i = 0; i < atributorCheckedListBox.Items.Count; i++) {

					if (oWooProductAttributeLine.WOOIDPRODUCTATTRIBUTELINE == ((WooProductAttribute)atributorCheckedListBox.Items[ i ]).WOOIDATRIBUTO) {
						atributorCheckedListBox.SetItemChecked( i, true );
					}
				}
			}
			atributorCheckedListBox.ItemCheck += atributorCheckedListBox_ItemCheck;
		}

		private void MostrarAtributosDelProducto() {
			atributosDelProductoDataGridView.DataSource = null;
			atributosDelProductoDataGridView.DataSource = productoSeleccionado.oWooProductAttributeLines;
		}

		public void cargarOrdenes() {
			Cursor.Current = Cursors.WaitCursor;

			if (todoRadioButton.Checked) {
				ordenesDataGridView.DataSource = MyWooCommerce.LeerTodasLasOrdenes();
			} else {

				BOResultadoWoo oBOResultadoWoo = MyWooCommerce.LeerOrdenesPendientes();
				ordenesDataGridView.DataSource = oBOResultadoWoo.Objeto as List<WooOrder>;
			}

			Cursor.Current = Cursors.Default;
		}

		public void cargarClientes() {
			Cursor.Current = Cursors.WaitCursor;
			clientesDataGridView.DataSource = MyWooCommerce.LeerTodosLosClientes();

			Cursor.Current = Cursors.Default;
		}

		public WooCustomer mostarDatosDelCliente( int id ) {
			BOResultadoWoo oResultadoWoo = MyWooCommerce.LeerClientePorID( id );

			if (oResultadoWoo.Exito) {
				oCliente = (WooCustomer)oResultadoWoo.Objeto;

				if (oCliente != null) {
					nombreTextBox.Text = oCliente.WOONOMBRE;
					apellidoTextBox.Text = oCliente.WOOAPELLIDO;
					eMailTextBox.Text = oCliente.WOOEMAIL;
					usuarioTextBox.Text = oCliente.WOOUSUARIO;
					claveTextBox.Text = oCliente.WOOCLAVE;
				}
			} else {
				oCliente = new WooCustomer();
			}
			return oCliente;
		}

		public void mostrarDatosOrderBilling( WooOrder oWooOrder ) {
			nombreOrderBillingtextBox.Text = oWooOrder.oWooOrderBilling.WOONOMBRE;
			apellidoOrderBillingTextBox.Text = oWooOrder.oWooOrderBilling.WOOAPELLIDO;
			companiaOrderBillingtextBox.Text = oWooOrder.oWooOrderBilling.WOOCOMPANIA;
			direccionOrderBillingTextBox.Text = oWooOrder.oWooOrderBilling.WOODIRECCION;
			codigoPostalOrderBillingTextBox.Text = oWooOrder.oWooOrderBilling.WOOCODIGOPOSTAL;
		}

		public void mostrarDatosOrderShipping( WooOrder oWooOrder ) {
			nombreOrderShipping_textBox.Text = oWooOrder.oWooOrderShipping.WOONOMBRE;
			apellidoOrderShipping_textBox.Text = oWooOrder.oWooOrderShipping.WOOAPELLIDO;
			companiaOrderShippingTextBox.Text = oWooOrder.oWooOrderShipping.WOOCOMPANIA;
			direccionOrderShipping_textBox.Text = oWooOrder.oWooOrderShipping.WOODIRECCION;
			codigoPostalOrderShipping_textBox.Text = oWooOrder.oWooOrderShipping.WOOCODIGOPOSTAL;
		}

		public void mostrarDatosCustomerBilling( WooCustomer oWooCustomer ) {
			nombreCustomerBillingTextBox.Text = oWooCustomer.oWooCustomerBilling.WOONOMBRE;
			apellidoCustomerBillingTextBox.Text = oWooCustomer.oWooCustomerBilling.WOOAPELLIDO;
			companiaCustomerBillingTextBox.Text = oWooCustomer.oWooCustomerBilling.WOOCOMPANIA;
			direccionCustomerBillingTextBox.Text = oWooCustomer.oWooCustomerBilling.WOODIRECCION;
			codigoPostalCustomerBillingTextBox.Text = oWooCustomer.oWooCustomerBilling.WOOCODIGOPOSTAL;
		}

		public void mostrarDatosCustomerShipping( WooCustomer oWooCustomer ) {
			nombreCustomerShippingTextBox.Text = oWooCustomer.oWooCustomerShipping.WOONOMBRE;
			apellidoCustomerShippingTextBox.Text = oWooCustomer.oWooCustomerShipping.WOOAPELLIDO;
			companiaCustomerShippingTextBox.Text = oWooCustomer.oWooCustomerShipping.WOOCOMPANIA;
			direccionCustomerShippingTextBox.Text = oWooCustomer.oWooCustomerShipping.WOODIRECCION;
			CodigoPostalCustomerShippingTextBox.Text = oWooCustomer.oWooCustomerShipping.WOOCODIGOPOSTAL;
		}

		public void guardarDatosDelCliente() {
			Cursor.Current = Cursors.WaitCursor;

			oCliente.WOONOMBRE = nombreTextBox.Text;
			oCliente.WOOAPELLIDO = apellidoTextBox.Text;
			oCliente.WOOEMAIL = eMailTextBox.Text;
			oCliente.WOOUSUARIO = usuarioTextBox.Text;
			oCliente.WOOCLAVE = claveTextBox.Text;

			MyWooCommerce.GrabarCliente( oCliente );

			oCliente = null;

			Cursor.Current = Cursors.Default;
			return;
		}

		public Image ImagenPrincipal( string URLImagen ) {
			if (URLImagen.Length == 0)
				return null;

			HttpWebRequest request = null;
			HttpWebResponse response = null;
			try {
				request = (HttpWebRequest)WebRequest.Create( URLImagen );
				response = (HttpWebResponse)request.GetResponse();

				Stream StreamImagen = response.GetResponseStream();
				return Image.FromStream( StreamImagen );
			} catch (Exception e) {
				return null;
			} finally {
				if (request != null)
					request.Abort();
				if (response != null)
					response.Close();
			}
		}

		public void mostrarProducto() {
			if (productoSeleccionado == null)
				return;

			textBoxNombreLargo.Text = productoSeleccionado.WOONOMBRELARGO;
			textBoxNombreCorto.Text = productoSeleccionado.WOONOMBRECORTO;
			richTextBoxCaracteristicas.Text = productoSeleccionado.WOOCARACTERISTICAS;
			textBoxURLImagen.Text = productoSeleccionado.WOOURLIMAGEN;
			pictureBoxProducto.Image = ImagenPrincipal( productoSeleccionado.WOOURLIMAGEN );
			textBoxPrecioDeVenta.Text = productoSeleccionado.WOOPRECIODEVENTA.ToString();
			checkBoxPublicar.Checked = productoSeleccionado.WOOPUBLICAR;
			productoEnOfertaCheckBox.Checked = productoSeleccionado.WOOPRODUCTOENOFERTA;
			inicioOfertadateTimePicker.Value = productoSeleccionado.WOOINICIOOFERTA;
			finalOfertaDateTimePicker.Value = productoSeleccionado.WOOFINALOFERTA;
			precioDeOfertaTextBox.Text = productoSeleccionado.WOOPRECIODEOFERTA.ToString();
			manejaExistenciaCheckBox.Checked = productoSeleccionado.WOOMANEJAEXISTENCIA;
			ExistenciaTextBox.Text = productoSeleccionado.WOOEXISTENCIA.ToString();
			totalDeVentasTextBox.Text = productoSeleccionado.WOOTOTALDEVENTAS.ToString();

			if (productoSeleccionado.WOOTYPE == WooProductType.SIMPLE)
				simpleRadioButton.Checked = true;

			else
				variableRadioButton.Checked = true;

			MarcarCategorias();
			MarcarAtributos();

			//List<WooVariation> oVariaciones = MyWooCommerce.LeerTodasLasVariacionesDeUnProducto( productoSeleccionado.WOOIDPRODUCTO );

			MostrarAtributosDelProducto();
			//atributosDelProductoDataGridView.DataSource = productoSeleccionado.oWooProductAttributeLines;

			//variantesDataGridView.DataSource = oVariaciones;

			mostrarVariantesDelProducto();

		}

		public void mostrarVariantesDelProducto() {
			List<WooVariation> oVariaciones = MyWooCommerce.LeerTodasLasVariacionesDeUnProducto( productoSeleccionado.WOOIDPRODUCTO );
			variantesDataGridView.DataSource = oVariaciones;
		}
		

		public void grabarProducto() {
			if (productoSeleccionado == null) {
				productoSeleccionado = new WooProduct();
			}

			productoSeleccionado.WOONOMBRELARGO = textBoxNombreLargo.Text;
			productoSeleccionado.WOONOMBRECORTO = textBoxNombreCorto.Text;
			productoSeleccionado.WOOCARACTERISTICAS = richTextBoxCaracteristicas.Text;
			productoSeleccionado.WOOURLIMAGEN = textBoxURLImagen.Text;
			productoSeleccionado.WOOPRECIODEVENTA = TextoADecimal( textBoxPrecioDeVenta.Text );
			productoSeleccionado.WOOPUBLICAR = checkBoxPublicar.Checked;
			productoSeleccionado.WOOPRODUCTOENOFERTA = productoEnOfertaCheckBox.Checked;
			productoSeleccionado.WOOINICIOOFERTA = inicioOfertadateTimePicker.Value.Date;
			productoSeleccionado.WOOFINALOFERTA = finalOfertaDateTimePicker.Value.Date;
			productoSeleccionado.WOOPRECIODEOFERTA = TextoADecimal( precioDeOfertaTextBox.Text );
			productoSeleccionado.WOOMANEJAEXISTENCIA = manejaExistenciaCheckBox.Checked;
			productoSeleccionado.WOOEXISTENCIA = (int)TextoADecimal( ExistenciaTextBox.Text );
			productoSeleccionado.WOOTOTALDEVENTAS = (long)TextoADecimal( totalDeVentasTextBox.Text );

			productoSeleccionado.EliminarTodasLasCategoria();

			foreach (WooCategory categoria in checkedListBoxCategorias.CheckedItems) {
				productoSeleccionado.AgregarCategoria( categoria.WOOIDCATEGORIA );
			}

			foreach (DataGridViewRow oDataGridViewRow in variantesDataGridView.Rows) {
				WooVariation oWooVariacion = (WooVariation)oDataGridViewRow.DataBoundItem;
				MyWooCommerce.GrabarVariacion( oWooVariacion, productoSeleccionado.WOOIDPRODUCTO );
			}

			if (simpleRadioButton.Checked)
				productoSeleccionado.WOOTYPE = WooProductType.SIMPLE;
			else
				productoSeleccionado.WOOTYPE = WooProductType.VARIABLE;

			MyWooCommerce.GrabarProducto( productoSeleccionado );
		}


		#region EVENTOS

		private void buttonProductos_Click( object sender, EventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;

			//dataGridViewProductos.DataSource = MyWooCommerce.LeerTodosLosProductos( "search", "nnnn" );
			//dataGridViewProductos.DataSource = MyWooCommerce.LeerTodosLosProductos( "on_sale", "false" );
			dataGridViewProductos.DataSource = MyWooCommerce.LeerTodosLosProductos( "", "" );

			Cursor.Current = Cursors.Default;
		}

		private decimal TextoADecimal( string Texto ) {
			if (Texto.Length > 0)
				return Convert.ToDecimal( Texto );
			else
				return 0;
		}

		private void buttonActualizarProductos_Click( object sender, EventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;
			grabarProducto();

			limpiarProducto();

			Cursor.Current = Cursors.Default;
			return;
		}

		private void dataGridViewProductos_CellClick( object sender, DataGridViewCellEventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;

			int IdSeleccionando = ((WooProduct)dataGridViewProductos.SelectedRows[ 0 ].DataBoundItem).WOOIDPRODUCTO;

			BOResultadoWoo oResultadoWoo = MyWooCommerce.LeerProductoPorID( IdSeleccionando );

			if (oResultadoWoo.Exito) {
				productoSeleccionado = (WooProduct)oResultadoWoo.Objeto;

				mostrarProducto();
			}

			Cursor.Current = Cursors.Default;
		}

		private void textBoxURLImagen_Validated( object sender, EventArgs e ) {
			if (productoSeleccionado != null && textBoxURLImagen.Text.Length > 0) {
				if (!Util.EsValidaLaImagen( textBoxURLImagen.Text )) {
					MessageBox.Show( "La imagen no es valida", "Mensaje", MessageBoxButtons.OK );
					pictureBoxProducto.Image = null;
					return;
				}
				productoSeleccionado.WOOURLIMAGEN = textBoxURLImagen.Text;
				pictureBoxProducto.Image = ImagenPrincipal( productoSeleccionado.WOOURLIMAGEN );
			}
		}

		private void ordenesDataGridView_CellClick( object sender, DataGridViewCellEventArgs e ) {
			WooOrder orden = (WooOrder)ordenesDataGridView.Rows[ e.RowIndex ].DataBoundItem;

			detallesOrdenDataGridView.DataSource = orden.oDetallesDeLaOrden;

			costoDelEnvioTextBox.Text = orden.WOOCOSTOENVIO.ToString( "N2" );

			WooCustomer oWooCustomer = mostarDatosDelCliente( orden.WOOIDCLIENTE );
			mostrarDatosOrderBilling( orden );
			mostrarDatosOrderShipping( orden );

			mostrarDatosCustomerBilling( oWooCustomer );
			mostrarDatosCustomerShipping( oWooCustomer );
		}

		private void todoRadioButton_CheckedChanged( object sender, EventArgs e ) {
			if (todoRadioButton.Checked)
				cargarOrdenes();
		}

		private void porProcesarradioButton_CheckedChanged( object sender, EventArgs e ) {
			if (porProcesarradioButton.Checked)
				cargarOrdenes();
		}

		private void tabControl1_Selected( object sender, TabControlEventArgs e ) {
			TabPage current = (sender as TabControl).SelectedTab;

			if (current.Name == "OrdenesTabPage") {
				cargarOrdenes();
			} else if (current.Name == "clientesTabPage") {
				cargarClientes();
			}
		}

		private void guardarCliente_Click( object sender, EventArgs e ) {
			guardarDatosDelCliente();
		}

		private void clientesDataGridView_CellClick( object sender, DataGridViewCellEventArgs e ) {
			WooCustomer oWooCustomer = clientesDataGridView.Rows[ e.RowIndex ].DataBoundItem as WooCustomer;

			mostarDatosDelCliente( oWooCustomer.WOOIDCLIENTE );
			mostrarDatosCustomerBilling( oWooCustomer );
			mostrarDatosCustomerShipping( oWooCustomer );
		}

		private void buttonEstablecerImagen_Click( object sender, EventArgs e ) {
			//productoSeleccionado

			BOResultadoWoo resp = MyWooCommerce.EstablecerImagenProduco( 201, textBoxURLImagen.Text );

		}

		private void label16_Click( object sender, EventArgs e ) {

		}

		private void label30_Click( object sender, EventArgs e ) {

		}

		private void codigoPostalCustomerBillingTextBox_TextChanged( object sender, EventArgs e ) {

		}

		private void button1_Click( object sender, EventArgs e ) {
			limpiarAtributo();
			atributosDataGridView.DataSource = MyWooCommerce.LeerTodosLosAtributos();
		}

		private void guardarAtributoButton_Click( object sender, EventArgs e ) {
			BOResultadoWoo oResultado = null;

			if (oWooAttribute == null)
				oWooAttribute = new WooProductAttribute();

			oWooAttribute.WOONOMBREDELATRIBUTO = NombreDelAtributo_TextBox.Text;

			oResultado = MyWooCommerce.GrabarAtributoDeProducto( oWooAttribute );

			if (oResultado.oWooError != null) {
				if (oResultado.oWooError.dataStatus == (int)WooErrorStatus.NoSePudoCrear);
				{

				}
			}

			limpiarAtributo();
			button1_Click( null, null );
		}

		private void atributosDataGridView_CellClick( object sender, DataGridViewCellEventArgs e ) {
			oWooAttribute = (WooProductAttribute)atributosDataGridView.SelectedRows[ 0 ].DataBoundItem;

			NombreDelAtributo_TextBox.Text = oWooAttribute.WOONOMBREDELATRIBUTO;

			terminosDataGridView.DataSource = MyWooCommerce.LeerTodosLosTerminosDelAtributo( oWooAttribute.WOOIDATRIBUTO );
		}

		private void button2_Click( object sender, EventArgs e ) {

			if (oWooAttributeTerm == null)
				oWooAttributeTerm = new WooProductAttributeTerm();

			oWooAttributeTerm.WOONOMBREDELTERMINO = nombreDelTerminoTextBox.Text;

			MyWooCommerce.GrabarTerminoDelAtributo( oWooAttributeTerm, oWooAttribute.WOOIDATRIBUTO );

			limpiarAtributo();
			button1_Click( null, null );
		}

		private void terminosDataGridView_CellClick( object sender, DataGridViewCellEventArgs e ) {
			oWooAttributeTerm = (WooProductAttributeTerm)terminosDataGridView.SelectedRows[ 0 ].DataBoundItem;

			nombreDelTerminoTextBox.Text = oWooAttributeTerm.WOONOMBREDELTERMINO;
		}

		private void eliminarTerminoButton_Click( object sender, EventArgs e ) {
			MyWooCommerce.EliminarTerminoDelAtributo( oWooAttributeTerm.WOOIDPRODUCATTRIBUTETERM, oWooAttribute.WOOIDATRIBUTO );

			limpiarAtributo();
		}

		private void eliminarAtributoBbutton_Click( object sender, EventArgs e ) {
			MyWooCommerce.EliminarAtributoDeProducto( oWooAttribute.WOOIDATRIBUTO );

			limpiarAtributo();
			button1_Click( null, null );
		}

		private void tabPage1_Click( object sender, EventArgs e ) {

		}

		private void eliminarVarianteButton_Click( object sender, EventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;
			foreach (int idVariacion in productoSeleccionado.oVariaciones) {
				MyWooCommerce.EliminarVariacion( idVariacion, productoSeleccionado.WOOIDPRODUCTO );
			}
			mostrarVariantesDelProducto();
			Cursor.Current = Cursors.Default;
		}

		private void agregarVarianteButton_Click( object sender, EventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;

			WooVariation oWooVariacion = new WooVariation();
			oWooVariacion.Precio = TextoADecimal( precioVarianteTextBox.Text );

			MyWooCommerce.GrabarVariacion( oWooVariacion, productoSeleccionado.WOOIDPRODUCTO );

			mostrarVariantesDelProducto();

			Cursor.Current = Cursors.Default;
		}

		#endregion

		private void atributosDelProductoDataGridView_CellClick( object sender, DataGridViewCellEventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;

			WooProductAttributeLine oWooProductAttributeLine = (WooProductAttributeLine)atributosDelProductoDataGridView.Rows[ e.RowIndex ].DataBoundItem;

			MostrarTerminos( oWooProductAttributeLine.WOOIDPRODUCTATTRIBUTELINE );
			MarcarTerminos( oWooProductAttributeLine,  oWooProductAttributeLine.Terminos );

			Cursor.Current = Cursors.Default;
		}

		private void atributorCheckedListBox_ItemCheck( object sender, ItemCheckEventArgs e ) {

			Cursor.Current = Cursors.WaitCursor;

			WooProductAttribute oWooProductAttribute = (WooProductAttribute)atributorCheckedListBox.Items[ e.Index ];
			if (e.NewValue == CheckState.Checked) {
				// Leo todos los términos para cargarlos todos por defecto
				List<WooProductAttributeTerm> oWooProductAttributeTerm = MyWooCommerce.LeerTodosLosTerminosDelAtributo( oWooProductAttribute.WOOIDATRIBUTO );
				MyWooCommerce.AgregarWooProductAttributeLine( productoSeleccionado, oWooProductAttribute.WOOIDATRIBUTO, oWooProductAttributeTerm );

			} else if (e.NewValue == CheckState.Unchecked) {
				if (oWooProductAttribute != null)
					MyWooCommerce.EliminarWooProductAttributeLine( productoSeleccionado, oWooProductAttribute.WOOIDATRIBUTO );
			}

			MostrarAtributosDelProducto();
			Cursor.Current = Cursors.Default;
		}

		private void terminosCheckedListBox_ItemCheck( object sender, ItemCheckEventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;

			WooProductAttributeLine oWooProductAttributeLine = (WooProductAttributeLine)terminosCheckedListBox.Tag;

			WooProductAttributeTerm oWooProductAttributeTerm = (WooProductAttributeTerm) terminosCheckedListBox.Items[e.Index];

			if (e.NewValue == CheckState.Checked) {
				MyWooCommerce.AgregarWooProductAttributeLineTerm( oWooProductAttributeLine, oWooProductAttributeTerm.WOONOMBREDELTERMINO );
			}
			else if (e.NewValue == CheckState.Unchecked) {
				MyWooCommerce.EliminarWooProductAttributeLineTerm( oWooProductAttributeLine, oWooProductAttributeTerm.WOONOMBREDELTERMINO );
			}

			Cursor.Current = Cursors.Default;
		}

		private void actualizarExistenciaButton_Click( object sender, EventArgs e ) {
			Cursor.Current = Cursors.WaitCursor;

			MyWooCommerce.ActualizarExistencia( productoSeleccionado.WOOIDPRODUCTO, TextoADecimal( ExistenciaTextBox.Text ) );

			Cursor.Current = Cursors.Default;
		}

		private void actualizarTodosLosProducto_Click( object sender, EventArgs e )
		{
			Cursor.Current = Cursors.WaitCursor;

			int i = 1;
			int contador = 0;
			bool continuar = true;

			regsitrosLeidos_textBox.Text = "0";

			while (continuar)
			{
				List<WooProduct> productos = MyWooCommerce.LeerTodosLosProductos( "page", i.ToString() );

				if (productos.Count == 0)
					continuar = false;
				else
				{
					dataGridViewProductos.DataSource = productos;

					foreach (WooProduct oWooProducto in productos)
					{
						contador++;
						regsitrosLeidos_textBox.Text = contador.ToString();

						if (oWooProducto.oProduct.on_sale.Value)
						{
							oWooProducto.oProduct.on_sale = false;

							oWooProducto.WOOIDPRODUCTO = oWooProducto.oProduct.id.Value;

							oWooProducto.oProduct = MyWooCommerce.oWCObject.Product.Update( oWooProducto.WOOIDPRODUCTO, oWooProducto.oProduct, new Dictionary<string, string>() ).Result;
						}

					}

				}
				i++;
			}

			Cursor.Current = Cursors.Default;
		}
	}
}
