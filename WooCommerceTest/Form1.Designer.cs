﻿namespace WooCommerceTest
{
	partial class Form1
	{
		/// <summary>
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.ordenesTabControl = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.label46 = new System.Windows.Forms.Label();
			this.terminosCheckedListBox = new System.Windows.Forms.CheckedListBox();
			this.variableRadioButton = new System.Windows.Forms.RadioButton();
			this.simpleRadioButton = new System.Windows.Forms.RadioButton();
			this.Variante = new System.Windows.Forms.GroupBox();
			this.agregarVarianteButton = new System.Windows.Forms.Button();
			this.precioVarianteTextBox = new System.Windows.Forms.TextBox();
			this.label45 = new System.Windows.Forms.Label();
			this.eliminarVarianteButton = new System.Windows.Forms.Button();
			this.eliminarAtributoButton = new System.Windows.Forms.Button();
			this.label44 = new System.Windows.Forms.Label();
			this.atributosDelProductoDataGridView = new System.Windows.Forms.DataGridView();
			this.variantesDataGridView = new System.Windows.Forms.DataGridView();
			this.label43 = new System.Windows.Forms.Label();
			this.label42 = new System.Windows.Forms.Label();
			this.atributorCheckedListBox = new System.Windows.Forms.CheckedListBox();
			this.buttonEstablecerImagen = new System.Windows.Forms.Button();
			this.versionTextBox = new System.Windows.Forms.TextBox();
			this.dataGridViewProductos = new System.Windows.Forms.DataGridView();
			this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colNombreProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.actualizarExistenciaButton = new System.Windows.Forms.Button();
			this.totalDeVentasTextBox = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.ExistenciaTextBox = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.manejaExistenciaCheckBox = new System.Windows.Forms.CheckBox();
			this.buttonProductos = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.precioDeOfertaTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.finalOfertaDateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.inicioOfertadateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.productoEnOfertaCheckBox = new System.Windows.Forms.CheckBox();
			this.richTextBoxCaracteristicas = new System.Windows.Forms.RichTextBox();
			this.checkBoxPublicar = new System.Windows.Forms.CheckBox();
			this.buttonActualizarProductos = new System.Windows.Forms.Button();
			this.textBoxPrecioDeVenta = new System.Windows.Forms.TextBox();
			this.pictureBoxProducto = new System.Windows.Forms.PictureBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxNombreLargo = new System.Windows.Forms.TextBox();
			this.checkedListBoxCategorias = new System.Windows.Forms.CheckedListBox();
			this.textBoxNombreCorto = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxURLImagen = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.OrdenesTabPage = new System.Windows.Forms.TabPage();
			this.costoDelEnvioTextBox = new System.Windows.Forms.TextBox();
			this.label39 = new System.Windows.Forms.Label();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.companiaOrderBillingtextBox = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.codigoPostalOrderBillingTextBox = new System.Windows.Forms.TextBox();
			this.label34 = new System.Windows.Forms.Label();
			this.apellidoOrderBillingTextBox = new System.Windows.Forms.TextBox();
			this.label35 = new System.Windows.Forms.Label();
			this.direccionOrderBillingTextBox = new System.Windows.Forms.TextBox();
			this.label36 = new System.Windows.Forms.Label();
			this.nombreOrderBillingtextBox = new System.Windows.Forms.TextBox();
			this.label37 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.companiaOrderShippingTextBox = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.codigoPostalOrderShipping_textBox = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.apellidoOrderShipping_textBox = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this.direccionOrderShipping_textBox = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.nombreOrderShipping_textBox = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.porProcesarradioButton = new System.Windows.Forms.RadioButton();
			this.todoRadioButton = new System.Windows.Forms.RadioButton();
			this.label12 = new System.Windows.Forms.Label();
			this.detallesOrdenDataGridView = new System.Windows.Forms.DataGridView();
			this.ordenesDataGridView = new System.Windows.Forms.DataGridView();
			this.clientesTabPage = new System.Windows.Forms.TabPage();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.companiaCustomerBillingTextBox = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.codigoPostalCustomerBillingTextBox = new System.Windows.Forms.TextBox();
			this.label30 = new System.Windows.Forms.Label();
			this.apellidoCustomerBillingTextBox = new System.Windows.Forms.TextBox();
			this.label31 = new System.Windows.Forms.Label();
			this.direccionCustomerBillingTextBox = new System.Windows.Forms.TextBox();
			this.label32 = new System.Windows.Forms.Label();
			this.nombreCustomerBillingTextBox = new System.Windows.Forms.TextBox();
			this.label33 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.companiaCustomerShippingTextBox = new System.Windows.Forms.TextBox();
			this.label38 = new System.Windows.Forms.Label();
			this.CodigoPostalCustomerShippingTextBox = new System.Windows.Forms.TextBox();
			this.label26 = new System.Windows.Forms.Label();
			this.apellidoCustomerShippingTextBox = new System.Windows.Forms.TextBox();
			this.label27 = new System.Windows.Forms.Label();
			this.direccionCustomerShippingTextBox = new System.Windows.Forms.TextBox();
			this.label28 = new System.Windows.Forms.Label();
			this.nombreCustomerShippingTextBox = new System.Windows.Forms.TextBox();
			this.label29 = new System.Windows.Forms.Label();
			this.claveTextBox = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.usuarioTextBox = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.guardarCliente = new System.Windows.Forms.Button();
			this.eMailTextBox = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.apellidoTextBox = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.nombreTextBox = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.clientesDataGridView = new System.Windows.Forms.DataGridView();
			this.atributosTabPage = new System.Windows.Forms.TabPage();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.eliminarAtributoBbutton = new System.Windows.Forms.Button();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.eliminarTerminoButton = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.nombreDelTerminoTextBox = new System.Windows.Forms.TextBox();
			this.label41 = new System.Windows.Forms.Label();
			this.terminosDataGridView = new System.Windows.Forms.DataGridView();
			this.guardarAtributoButton = new System.Windows.Forms.Button();
			this.NombreDelAtributo_TextBox = new System.Windows.Forms.TextBox();
			this.label40 = new System.Windows.Forms.Label();
			this.atributosDataGridView = new System.Windows.Forms.DataGridView();
			this.button1 = new System.Windows.Forms.Button();
			this.actualizarTodosLosProducto = new System.Windows.Forms.Button();
			this.regsitrosLeidos_textBox = new System.Windows.Forms.TextBox();
			this.label47 = new System.Windows.Forms.Label();
			this.ordenesTabControl.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.Variante.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.atributosDelProductoDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.variantesDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProductos)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxProducto)).BeginInit();
			this.OrdenesTabPage.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.detallesOrdenDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ordenesDataGridView)).BeginInit();
			this.clientesTabPage.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
			this.atributosTabPage.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.terminosDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.atributosDataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// ordenesTabControl
			// 
			this.ordenesTabControl.Controls.Add(this.tabPage1);
			this.ordenesTabControl.Controls.Add(this.OrdenesTabPage);
			this.ordenesTabControl.Controls.Add(this.clientesTabPage);
			this.ordenesTabControl.Controls.Add(this.atributosTabPage);
			this.ordenesTabControl.Location = new System.Drawing.Point(12, 12);
			this.ordenesTabControl.Name = "ordenesTabControl";
			this.ordenesTabControl.SelectedIndex = 0;
			this.ordenesTabControl.Size = new System.Drawing.Size(1435, 648);
			this.ordenesTabControl.TabIndex = 0;
			this.ordenesTabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.label47);
			this.tabPage1.Controls.Add(this.regsitrosLeidos_textBox);
			this.tabPage1.Controls.Add(this.actualizarTodosLosProducto);
			this.tabPage1.Controls.Add(this.label46);
			this.tabPage1.Controls.Add(this.terminosCheckedListBox);
			this.tabPage1.Controls.Add(this.variableRadioButton);
			this.tabPage1.Controls.Add(this.simpleRadioButton);
			this.tabPage1.Controls.Add(this.Variante);
			this.tabPage1.Controls.Add(this.eliminarVarianteButton);
			this.tabPage1.Controls.Add(this.eliminarAtributoButton);
			this.tabPage1.Controls.Add(this.label44);
			this.tabPage1.Controls.Add(this.atributosDelProductoDataGridView);
			this.tabPage1.Controls.Add(this.variantesDataGridView);
			this.tabPage1.Controls.Add(this.label43);
			this.tabPage1.Controls.Add(this.label42);
			this.tabPage1.Controls.Add(this.atributorCheckedListBox);
			this.tabPage1.Controls.Add(this.buttonEstablecerImagen);
			this.tabPage1.Controls.Add(this.versionTextBox);
			this.tabPage1.Controls.Add(this.dataGridViewProductos);
			this.tabPage1.Controls.Add(this.groupBox2);
			this.tabPage1.Controls.Add(this.buttonProductos);
			this.tabPage1.Controls.Add(this.groupBox1);
			this.tabPage1.Controls.Add(this.richTextBoxCaracteristicas);
			this.tabPage1.Controls.Add(this.checkBoxPublicar);
			this.tabPage1.Controls.Add(this.buttonActualizarProductos);
			this.tabPage1.Controls.Add(this.textBoxPrecioDeVenta);
			this.tabPage1.Controls.Add(this.pictureBoxProducto);
			this.tabPage1.Controls.Add(this.label7);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Controls.Add(this.textBoxNombreLargo);
			this.tabPage1.Controls.Add(this.checkedListBoxCategorias);
			this.tabPage1.Controls.Add(this.textBoxNombreCorto);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label5);
			this.tabPage1.Controls.Add(this.textBoxURLImagen);
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(1427, 622);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Productos";
			this.tabPage1.UseVisualStyleBackColor = true;
			this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
			// 
			// label46
			// 
			this.label46.AutoSize = true;
			this.label46.Location = new System.Drawing.Point(1201, 193);
			this.label46.Name = "label46";
			this.label46.Size = new System.Drawing.Size(50, 13);
			this.label46.TabIndex = 27;
			this.label46.Text = "Terminos";
			// 
			// terminosCheckedListBox
			// 
			this.terminosCheckedListBox.CheckOnClick = true;
			this.terminosCheckedListBox.FormattingEnabled = true;
			this.terminosCheckedListBox.Location = new System.Drawing.Point(1204, 217);
			this.terminosCheckedListBox.Name = "terminosCheckedListBox";
			this.terminosCheckedListBox.Size = new System.Drawing.Size(187, 79);
			this.terminosCheckedListBox.TabIndex = 26;
			this.terminosCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.terminosCheckedListBox_ItemCheck);
			// 
			// variableRadioButton
			// 
			this.variableRadioButton.AutoSize = true;
			this.variableRadioButton.Location = new System.Drawing.Point(904, 57);
			this.variableRadioButton.Name = "variableRadioButton";
			this.variableRadioButton.Size = new System.Drawing.Size(63, 17);
			this.variableRadioButton.TabIndex = 25;
			this.variableRadioButton.Text = "Variable";
			this.variableRadioButton.UseVisualStyleBackColor = true;
			// 
			// simpleRadioButton
			// 
			this.simpleRadioButton.AutoSize = true;
			this.simpleRadioButton.Checked = true;
			this.simpleRadioButton.Location = new System.Drawing.Point(904, 30);
			this.simpleRadioButton.Name = "simpleRadioButton";
			this.simpleRadioButton.Size = new System.Drawing.Size(56, 17);
			this.simpleRadioButton.TabIndex = 24;
			this.simpleRadioButton.TabStop = true;
			this.simpleRadioButton.Text = "Simple";
			this.simpleRadioButton.UseVisualStyleBackColor = true;
			// 
			// Variante
			// 
			this.Variante.Controls.Add(this.agregarVarianteButton);
			this.Variante.Controls.Add(this.precioVarianteTextBox);
			this.Variante.Controls.Add(this.label45);
			this.Variante.Location = new System.Drawing.Point(901, 544);
			this.Variante.Name = "Variante";
			this.Variante.Size = new System.Drawing.Size(369, 71);
			this.Variante.TabIndex = 23;
			this.Variante.TabStop = false;
			this.Variante.Text = "Variante";
			// 
			// agregarVarianteButton
			// 
			this.agregarVarianteButton.Location = new System.Drawing.Point(153, 35);
			this.agregarVarianteButton.Name = "agregarVarianteButton";
			this.agregarVarianteButton.Size = new System.Drawing.Size(92, 23);
			this.agregarVarianteButton.TabIndex = 24;
			this.agregarVarianteButton.Text = "Agregar variante";
			this.agregarVarianteButton.UseVisualStyleBackColor = true;
			this.agregarVarianteButton.Click += new System.EventHandler(this.agregarVarianteButton_Click);
			// 
			// precioVarianteTextBox
			// 
			this.precioVarianteTextBox.Location = new System.Drawing.Point(6, 38);
			this.precioVarianteTextBox.Name = "precioVarianteTextBox";
			this.precioVarianteTextBox.Size = new System.Drawing.Size(118, 20);
			this.precioVarianteTextBox.TabIndex = 24;
			// 
			// label45
			// 
			this.label45.AutoSize = true;
			this.label45.Location = new System.Drawing.Point(6, 22);
			this.label45.Name = "label45";
			this.label45.Size = new System.Drawing.Size(82, 13);
			this.label45.TabIndex = 25;
			this.label45.Text = "Precio de venta";
			// 
			// eliminarVarianteButton
			// 
			this.eliminarVarianteButton.Location = new System.Drawing.Point(901, 450);
			this.eliminarVarianteButton.Name = "eliminarVarianteButton";
			this.eliminarVarianteButton.Size = new System.Drawing.Size(92, 23);
			this.eliminarVarianteButton.TabIndex = 22;
			this.eliminarVarianteButton.Text = "Eliminar variante";
			this.eliminarVarianteButton.UseVisualStyleBackColor = true;
			this.eliminarVarianteButton.Click += new System.EventHandler(this.eliminarVarianteButton_Click);
			// 
			// eliminarAtributoButton
			// 
			this.eliminarAtributoButton.Location = new System.Drawing.Point(901, 302);
			this.eliminarAtributoButton.Name = "eliminarAtributoButton";
			this.eliminarAtributoButton.Size = new System.Drawing.Size(98, 23);
			this.eliminarAtributoButton.TabIndex = 21;
			this.eliminarAtributoButton.Text = "Eliminar atributo";
			this.eliminarAtributoButton.UseVisualStyleBackColor = true;
			// 
			// label44
			// 
			this.label44.AutoSize = true;
			this.label44.Location = new System.Drawing.Point(898, 193);
			this.label44.Name = "label44";
			this.label44.Size = new System.Drawing.Size(110, 13);
			this.label44.TabIndex = 20;
			this.label44.Text = "Atributos del producto";
			// 
			// atributosDelProductoDataGridView
			// 
			this.atributosDelProductoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.atributosDelProductoDataGridView.Location = new System.Drawing.Point(901, 209);
			this.atributosDelProductoDataGridView.Name = "atributosDelProductoDataGridView";
			this.atributosDelProductoDataGridView.RowHeadersVisible = false;
			this.atributosDelProductoDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.atributosDelProductoDataGridView.Size = new System.Drawing.Size(288, 87);
			this.atributosDelProductoDataGridView.TabIndex = 19;
			this.atributosDelProductoDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.atributosDelProductoDataGridView_CellClick);
			// 
			// variantesDataGridView
			// 
			this.variantesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.variantesDataGridView.Location = new System.Drawing.Point(901, 357);
			this.variantesDataGridView.Name = "variantesDataGridView";
			this.variantesDataGridView.RowHeadersVisible = false;
			this.variantesDataGridView.Size = new System.Drawing.Size(395, 87);
			this.variantesDataGridView.TabIndex = 18;
			// 
			// label43
			// 
			this.label43.AutoSize = true;
			this.label43.Location = new System.Drawing.Point(898, 341);
			this.label43.Name = "label43";
			this.label43.Size = new System.Drawing.Size(51, 13);
			this.label43.TabIndex = 17;
			this.label43.Text = "Variantes";
			// 
			// label42
			// 
			this.label42.AutoSize = true;
			this.label42.Location = new System.Drawing.Point(898, 84);
			this.label42.Name = "label42";
			this.label42.Size = new System.Drawing.Size(48, 13);
			this.label42.TabIndex = 16;
			this.label42.Text = "Atributos";
			// 
			// atributorCheckedListBox
			// 
			this.atributorCheckedListBox.CheckOnClick = true;
			this.atributorCheckedListBox.FormattingEnabled = true;
			this.atributorCheckedListBox.Location = new System.Drawing.Point(901, 102);
			this.atributorCheckedListBox.Name = "atributorCheckedListBox";
			this.atributorCheckedListBox.Size = new System.Drawing.Size(220, 79);
			this.atributorCheckedListBox.TabIndex = 15;
			this.atributorCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.atributorCheckedListBox_ItemCheck);
			// 
			// buttonEstablecerImagen
			// 
			this.buttonEstablecerImagen.Location = new System.Drawing.Point(1136, 2);
			this.buttonEstablecerImagen.Name = "buttonEstablecerImagen";
			this.buttonEstablecerImagen.Size = new System.Drawing.Size(111, 23);
			this.buttonEstablecerImagen.TabIndex = 13;
			this.buttonEstablecerImagen.Text = "Establecer Imagen";
			this.buttonEstablecerImagen.UseVisualStyleBackColor = true;
			this.buttonEstablecerImagen.Click += new System.EventHandler(this.buttonEstablecerImagen_Click);
			// 
			// versionTextBox
			// 
			this.versionTextBox.Location = new System.Drawing.Point(18, 524);
			this.versionTextBox.Name = "versionTextBox";
			this.versionTextBox.Size = new System.Drawing.Size(118, 20);
			this.versionTextBox.TabIndex = 12;
			// 
			// dataGridViewProductos
			// 
			this.dataGridViewProductos.AllowUserToAddRows = false;
			this.dataGridViewProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colNombreProducto});
			this.dataGridViewProductos.Location = new System.Drawing.Point(18, 11);
			this.dataGridViewProductos.Name = "dataGridViewProductos";
			this.dataGridViewProductos.ReadOnly = true;
			this.dataGridViewProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewProductos.Size = new System.Drawing.Size(414, 495);
			this.dataGridViewProductos.TabIndex = 9;
			this.dataGridViewProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProductos_CellClick);
			// 
			// colID
			// 
			this.colID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.colID.HeaderText = "ID";
			this.colID.Name = "colID";
			this.colID.ReadOnly = true;
			this.colID.Width = 43;
			// 
			// colNombreProducto
			// 
			this.colNombreProducto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.colNombreProducto.HeaderText = "Nombre del producto";
			this.colNombreProducto.Name = "colNombreProducto";
			this.colNombreProducto.ReadOnly = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.actualizarExistenciaButton);
			this.groupBox2.Controls.Add(this.totalDeVentasTextBox);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Controls.Add(this.ExistenciaTextBox);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.manejaExistenciaCheckBox);
			this.groupBox2.Location = new System.Drawing.Point(632, 404);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(240, 198);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Manejo de existencias";
			// 
			// actualizarExistenciaButton
			// 
			this.actualizarExistenciaButton.Location = new System.Drawing.Point(13, 140);
			this.actualizarExistenciaButton.Name = "actualizarExistenciaButton";
			this.actualizarExistenciaButton.Size = new System.Drawing.Size(178, 23);
			this.actualizarExistenciaButton.TabIndex = 28;
			this.actualizarExistenciaButton.Text = "Actualizar existencia";
			this.actualizarExistenciaButton.UseVisualStyleBackColor = true;
			this.actualizarExistenciaButton.Click += new System.EventHandler(this.actualizarExistenciaButton_Click);
			// 
			// totalDeVentasTextBox
			// 
			this.totalDeVentasTextBox.Location = new System.Drawing.Point(13, 108);
			this.totalDeVentasTextBox.Name = "totalDeVentasTextBox";
			this.totalDeVentasTextBox.Size = new System.Drawing.Size(118, 20);
			this.totalDeVentasTextBox.TabIndex = 4;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(13, 92);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(81, 13);
			this.label11.TabIndex = 3;
			this.label11.Text = "Total de ventas";
			// 
			// ExistenciaTextBox
			// 
			this.ExistenciaTextBox.Location = new System.Drawing.Point(13, 62);
			this.ExistenciaTextBox.Name = "ExistenciaTextBox";
			this.ExistenciaTextBox.Size = new System.Drawing.Size(118, 20);
			this.ExistenciaTextBox.TabIndex = 2;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(13, 46);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(55, 13);
			this.label10.TabIndex = 1;
			this.label10.Text = "Existencia";
			// 
			// manejaExistenciaCheckBox
			// 
			this.manejaExistenciaCheckBox.AutoSize = true;
			this.manejaExistenciaCheckBox.Location = new System.Drawing.Point(13, 24);
			this.manejaExistenciaCheckBox.Name = "manejaExistenciaCheckBox";
			this.manejaExistenciaCheckBox.Size = new System.Drawing.Size(116, 17);
			this.manejaExistenciaCheckBox.TabIndex = 0;
			this.manejaExistenciaCheckBox.Text = "Maneja existencias";
			this.manejaExistenciaCheckBox.UseVisualStyleBackColor = true;
			// 
			// buttonProductos
			// 
			this.buttonProductos.Location = new System.Drawing.Point(291, 515);
			this.buttonProductos.Name = "buttonProductos";
			this.buttonProductos.Size = new System.Drawing.Size(141, 23);
			this.buttonProductos.TabIndex = 8;
			this.buttonProductos.Text = "Cargar lista de productos";
			this.buttonProductos.UseVisualStyleBackColor = true;
			this.buttonProductos.Click += new System.EventHandler(this.buttonProductos_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.precioDeOfertaTextBox);
			this.groupBox1.Controls.Add(this.label9);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.finalOfertaDateTimePicker);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.inicioOfertadateTimePicker);
			this.groupBox1.Controls.Add(this.productoEnOfertaCheckBox);
			this.groupBox1.Location = new System.Drawing.Point(632, 188);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(240, 183);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Oferta";
			// 
			// precioDeOfertaTextBox
			// 
			this.precioDeOfertaTextBox.Location = new System.Drawing.Point(13, 150);
			this.precioDeOfertaTextBox.Name = "precioDeOfertaTextBox";
			this.precioDeOfertaTextBox.Size = new System.Drawing.Size(118, 20);
			this.precioDeOfertaTextBox.TabIndex = 6;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(13, 134);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(82, 13);
			this.label9.TabIndex = 5;
			this.label9.Text = "Precio de oferta";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(13, 87);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(62, 13);
			this.label8.TabIndex = 3;
			this.label8.Text = "Finalización";
			// 
			// finalOfertaDateTimePicker
			// 
			this.finalOfertaDateTimePicker.Location = new System.Drawing.Point(13, 103);
			this.finalOfertaDateTimePicker.Name = "finalOfertaDateTimePicker";
			this.finalOfertaDateTimePicker.Size = new System.Drawing.Size(200, 20);
			this.finalOfertaDateTimePicker.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 42);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(32, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Inicio";
			// 
			// inicioOfertadateTimePicker
			// 
			this.inicioOfertadateTimePicker.Location = new System.Drawing.Point(13, 58);
			this.inicioOfertadateTimePicker.Name = "inicioOfertadateTimePicker";
			this.inicioOfertadateTimePicker.Size = new System.Drawing.Size(200, 20);
			this.inicioOfertadateTimePicker.TabIndex = 2;
			// 
			// productoEnOfertaCheckBox
			// 
			this.productoEnOfertaCheckBox.AutoSize = true;
			this.productoEnOfertaCheckBox.Location = new System.Drawing.Point(13, 19);
			this.productoEnOfertaCheckBox.Name = "productoEnOfertaCheckBox";
			this.productoEnOfertaCheckBox.Size = new System.Drawing.Size(69, 17);
			this.productoEnOfertaCheckBox.TabIndex = 0;
			this.productoEnOfertaCheckBox.Text = "En oferta";
			this.productoEnOfertaCheckBox.UseVisualStyleBackColor = true;
			// 
			// richTextBoxCaracteristicas
			// 
			this.richTextBoxCaracteristicas.Location = new System.Drawing.Point(438, 72);
			this.richTextBoxCaracteristicas.Name = "richTextBoxCaracteristicas";
			this.richTextBoxCaracteristicas.Size = new System.Drawing.Size(434, 61);
			this.richTextBoxCaracteristicas.TabIndex = 2;
			this.richTextBoxCaracteristicas.Text = "";
			// 
			// checkBoxPublicar
			// 
			this.checkBoxPublicar.AutoSize = true;
			this.checkBoxPublicar.Location = new System.Drawing.Point(438, 243);
			this.checkBoxPublicar.Name = "checkBoxPublicar";
			this.checkBoxPublicar.Size = new System.Drawing.Size(64, 17);
			this.checkBoxPublicar.TabIndex = 5;
			this.checkBoxPublicar.Text = "Publicar";
			this.checkBoxPublicar.UseVisualStyleBackColor = true;
			// 
			// buttonActualizarProductos
			// 
			this.buttonActualizarProductos.Location = new System.Drawing.Point(436, 516);
			this.buttonActualizarProductos.Name = "buttonActualizarProductos";
			this.buttonActualizarProductos.Size = new System.Drawing.Size(178, 23);
			this.buttonActualizarProductos.TabIndex = 7;
			this.buttonActualizarProductos.Text = "Actualizar Productos";
			this.buttonActualizarProductos.UseVisualStyleBackColor = true;
			this.buttonActualizarProductos.Click += new System.EventHandler(this.buttonActualizarProductos_Click);
			// 
			// textBoxPrecioDeVenta
			// 
			this.textBoxPrecioDeVenta.Location = new System.Drawing.Point(438, 204);
			this.textBoxPrecioDeVenta.Name = "textBoxPrecioDeVenta";
			this.textBoxPrecioDeVenta.Size = new System.Drawing.Size(118, 20);
			this.textBoxPrecioDeVenta.TabIndex = 4;
			// 
			// pictureBoxProducto
			// 
			this.pictureBoxProducto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBoxProducto.Location = new System.Drawing.Point(1136, 30);
			this.pictureBoxProducto.Name = "pictureBoxProducto";
			this.pictureBoxProducto.Size = new System.Drawing.Size(160, 151);
			this.pictureBoxProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBoxProducto.TabIndex = 5;
			this.pictureBoxProducto.TabStop = false;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(438, 188);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(82, 13);
			this.label7.TabIndex = 8;
			this.label7.Text = "Precio de venta";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(438, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 13);
			this.label2.TabIndex = 0;
			this.label2.Text = "Nombre largo";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(438, 56);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(78, 13);
			this.label6.TabIndex = 4;
			this.label6.Text = "Características";
			// 
			// textBoxNombreLargo
			// 
			this.textBoxNombreLargo.Location = new System.Drawing.Point(438, 25);
			this.textBoxNombreLargo.Name = "textBoxNombreLargo";
			this.textBoxNombreLargo.Size = new System.Drawing.Size(249, 20);
			this.textBoxNombreLargo.TabIndex = 0;
			// 
			// checkedListBoxCategorias
			// 
			this.checkedListBoxCategorias.CheckOnClick = true;
			this.checkedListBoxCategorias.FormattingEnabled = true;
			this.checkedListBoxCategorias.Location = new System.Drawing.Point(436, 301);
			this.checkedListBoxCategorias.Name = "checkedListBoxCategorias";
			this.checkedListBoxCategorias.Size = new System.Drawing.Size(178, 199);
			this.checkedListBoxCategorias.TabIndex = 6;
			// 
			// textBoxNombreCorto
			// 
			this.textBoxNombreCorto.Location = new System.Drawing.Point(693, 25);
			this.textBoxNombreCorto.Name = "textBoxNombreCorto";
			this.textBoxNombreCorto.Size = new System.Drawing.Size(179, 20);
			this.textBoxNombreCorto.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(438, 140);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(67, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "URL Imagen";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(690, 9);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(71, 13);
			this.label5.TabIndex = 1;
			this.label5.Text = "Nombre corto";
			// 
			// textBoxURLImagen
			// 
			this.textBoxURLImagen.Location = new System.Drawing.Point(438, 156);
			this.textBoxURLImagen.Name = "textBoxURLImagen";
			this.textBoxURLImagen.Size = new System.Drawing.Size(434, 20);
			this.textBoxURLImagen.TabIndex = 3;
			this.textBoxURLImagen.Validated += new System.EventHandler(this.textBoxURLImagen_Validated);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(438, 283);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(57, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Categorias";
			// 
			// OrdenesTabPage
			// 
			this.OrdenesTabPage.Controls.Add(this.costoDelEnvioTextBox);
			this.OrdenesTabPage.Controls.Add(this.label39);
			this.OrdenesTabPage.Controls.Add(this.groupBox6);
			this.OrdenesTabPage.Controls.Add(this.groupBox3);
			this.OrdenesTabPage.Controls.Add(this.porProcesarradioButton);
			this.OrdenesTabPage.Controls.Add(this.todoRadioButton);
			this.OrdenesTabPage.Controls.Add(this.label12);
			this.OrdenesTabPage.Controls.Add(this.detallesOrdenDataGridView);
			this.OrdenesTabPage.Controls.Add(this.ordenesDataGridView);
			this.OrdenesTabPage.Location = new System.Drawing.Point(4, 22);
			this.OrdenesTabPage.Name = "OrdenesTabPage";
			this.OrdenesTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.OrdenesTabPage.Size = new System.Drawing.Size(1427, 622);
			this.OrdenesTabPage.TabIndex = 1;
			this.OrdenesTabPage.Text = "Órdenes";
			this.OrdenesTabPage.UseVisualStyleBackColor = true;
			// 
			// costoDelEnvioTextBox
			// 
			this.costoDelEnvioTextBox.Location = new System.Drawing.Point(1077, 195);
			this.costoDelEnvioTextBox.Name = "costoDelEnvioTextBox";
			this.costoDelEnvioTextBox.Size = new System.Drawing.Size(102, 20);
			this.costoDelEnvioTextBox.TabIndex = 53;
			// 
			// label39
			// 
			this.label39.AutoSize = true;
			this.label39.Location = new System.Drawing.Point(991, 198);
			this.label39.Name = "label39";
			this.label39.Size = new System.Drawing.Size(80, 13);
			this.label39.TabIndex = 52;
			this.label39.Text = "Costo del envio";
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.companiaOrderBillingtextBox);
			this.groupBox6.Controls.Add(this.label16);
			this.groupBox6.Controls.Add(this.codigoPostalOrderBillingTextBox);
			this.groupBox6.Controls.Add(this.label34);
			this.groupBox6.Controls.Add(this.apellidoOrderBillingTextBox);
			this.groupBox6.Controls.Add(this.label35);
			this.groupBox6.Controls.Add(this.direccionOrderBillingTextBox);
			this.groupBox6.Controls.Add(this.label36);
			this.groupBox6.Controls.Add(this.nombreOrderBillingtextBox);
			this.groupBox6.Controls.Add(this.label37);
			this.groupBox6.Location = new System.Drawing.Point(578, 221);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(545, 134);
			this.groupBox6.TabIndex = 51;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Billing";
			// 
			// companiaOrderBillingtextBox
			// 
			this.companiaOrderBillingtextBox.Location = new System.Drawing.Point(96, 48);
			this.companiaOrderBillingtextBox.Name = "companiaOrderBillingtextBox";
			this.companiaOrderBillingtextBox.Size = new System.Drawing.Size(387, 20);
			this.companiaOrderBillingtextBox.TabIndex = 51;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(41, 50);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(54, 13);
			this.label16.TabIndex = 50;
			this.label16.Text = "Compañia";
			this.label16.Click += new System.EventHandler(this.label16_Click);
			// 
			// codigoPostalOrderBillingTextBox
			// 
			this.codigoPostalOrderBillingTextBox.Location = new System.Drawing.Point(96, 108);
			this.codigoPostalOrderBillingTextBox.Name = "codigoPostalOrderBillingTextBox";
			this.codigoPostalOrderBillingTextBox.Size = new System.Drawing.Size(147, 20);
			this.codigoPostalOrderBillingTextBox.TabIndex = 49;
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Location = new System.Drawing.Point(19, 111);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(71, 13);
			this.label34.TabIndex = 48;
			this.label34.Text = "Código postal";
			// 
			// apellidoOrderBillingTextBox
			// 
			this.apellidoOrderBillingTextBox.Location = new System.Drawing.Point(325, 22);
			this.apellidoOrderBillingTextBox.Name = "apellidoOrderBillingTextBox";
			this.apellidoOrderBillingTextBox.Size = new System.Drawing.Size(202, 20);
			this.apellidoOrderBillingTextBox.TabIndex = 47;
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Location = new System.Drawing.Point(267, 25);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(44, 13);
			this.label35.TabIndex = 46;
			this.label35.Text = "Apellido";
			// 
			// direccionOrderBillingTextBox
			// 
			this.direccionOrderBillingTextBox.Location = new System.Drawing.Point(96, 76);
			this.direccionOrderBillingTextBox.Name = "direccionOrderBillingTextBox";
			this.direccionOrderBillingTextBox.Size = new System.Drawing.Size(387, 20);
			this.direccionOrderBillingTextBox.TabIndex = 47;
			// 
			// label36
			// 
			this.label36.AutoSize = true;
			this.label36.Location = new System.Drawing.Point(38, 76);
			this.label36.Name = "label36";
			this.label36.Size = new System.Drawing.Size(52, 13);
			this.label36.TabIndex = 46;
			this.label36.Text = "Dirección";
			// 
			// nombreOrderBillingtextBox
			// 
			this.nombreOrderBillingtextBox.Location = new System.Drawing.Point(96, 22);
			this.nombreOrderBillingtextBox.Name = "nombreOrderBillingtextBox";
			this.nombreOrderBillingtextBox.Size = new System.Drawing.Size(165, 20);
			this.nombreOrderBillingtextBox.TabIndex = 47;
			// 
			// label37
			// 
			this.label37.AutoSize = true;
			this.label37.Location = new System.Drawing.Point(46, 25);
			this.label37.Name = "label37";
			this.label37.Size = new System.Drawing.Size(44, 13);
			this.label37.TabIndex = 46;
			this.label37.Text = "Nombre";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.companiaOrderShippingTextBox);
			this.groupBox3.Controls.Add(this.label17);
			this.groupBox3.Controls.Add(this.codigoPostalOrderShipping_textBox);
			this.groupBox3.Controls.Add(this.label25);
			this.groupBox3.Controls.Add(this.apellidoOrderShipping_textBox);
			this.groupBox3.Controls.Add(this.label24);
			this.groupBox3.Controls.Add(this.direccionOrderShipping_textBox);
			this.groupBox3.Controls.Add(this.label23);
			this.groupBox3.Controls.Add(this.nombreOrderShipping_textBox);
			this.groupBox3.Controls.Add(this.label22);
			this.groupBox3.Location = new System.Drawing.Point(578, 372);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(541, 140);
			this.groupBox3.TabIndex = 46;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Shipping";
			// 
			// companiaOrderShippingTextBox
			// 
			this.companiaOrderShippingTextBox.Location = new System.Drawing.Point(99, 54);
			this.companiaOrderShippingTextBox.Name = "companiaOrderShippingTextBox";
			this.companiaOrderShippingTextBox.Size = new System.Drawing.Size(387, 20);
			this.companiaOrderShippingTextBox.TabIndex = 53;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(44, 56);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(54, 13);
			this.label17.TabIndex = 52;
			this.label17.Text = "Compañia";
			// 
			// codigoPostalOrderShipping_textBox
			// 
			this.codigoPostalOrderShipping_textBox.Location = new System.Drawing.Point(99, 112);
			this.codigoPostalOrderShipping_textBox.Name = "codigoPostalOrderShipping_textBox";
			this.codigoPostalOrderShipping_textBox.Size = new System.Drawing.Size(147, 20);
			this.codigoPostalOrderShipping_textBox.TabIndex = 49;
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(22, 115);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(71, 13);
			this.label25.TabIndex = 48;
			this.label25.Text = "Código postal";
			// 
			// apellidoOrderShipping_textBox
			// 
			this.apellidoOrderShipping_textBox.Location = new System.Drawing.Point(320, 28);
			this.apellidoOrderShipping_textBox.Name = "apellidoOrderShipping_textBox";
			this.apellidoOrderShipping_textBox.Size = new System.Drawing.Size(202, 20);
			this.apellidoOrderShipping_textBox.TabIndex = 47;
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(270, 28);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(44, 13);
			this.label24.TabIndex = 46;
			this.label24.Text = "Apellido";
			// 
			// direccionOrderShipping_textBox
			// 
			this.direccionOrderShipping_textBox.Location = new System.Drawing.Point(99, 86);
			this.direccionOrderShipping_textBox.Name = "direccionOrderShipping_textBox";
			this.direccionOrderShipping_textBox.Size = new System.Drawing.Size(423, 20);
			this.direccionOrderShipping_textBox.TabIndex = 47;
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(41, 89);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(52, 13);
			this.label23.TabIndex = 46;
			this.label23.Text = "Dirección";
			// 
			// nombreOrderShipping_textBox
			// 
			this.nombreOrderShipping_textBox.Location = new System.Drawing.Point(99, 25);
			this.nombreOrderShipping_textBox.Name = "nombreOrderShipping_textBox";
			this.nombreOrderShipping_textBox.Size = new System.Drawing.Size(165, 20);
			this.nombreOrderShipping_textBox.TabIndex = 47;
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(49, 25);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(44, 13);
			this.label22.TabIndex = 46;
			this.label22.Text = "Nombre";
			// 
			// porProcesarradioButton
			// 
			this.porProcesarradioButton.AutoSize = true;
			this.porProcesarradioButton.Location = new System.Drawing.Point(72, 524);
			this.porProcesarradioButton.Name = "porProcesarradioButton";
			this.porProcesarradioButton.Size = new System.Drawing.Size(85, 17);
			this.porProcesarradioButton.TabIndex = 8;
			this.porProcesarradioButton.Text = "Por procesar";
			this.porProcesarradioButton.UseVisualStyleBackColor = true;
			this.porProcesarradioButton.CheckedChanged += new System.EventHandler(this.porProcesarradioButton_CheckedChanged);
			// 
			// todoRadioButton
			// 
			this.todoRadioButton.AutoSize = true;
			this.todoRadioButton.Checked = true;
			this.todoRadioButton.Location = new System.Drawing.Point(16, 524);
			this.todoRadioButton.Name = "todoRadioButton";
			this.todoRadioButton.Size = new System.Drawing.Size(50, 17);
			this.todoRadioButton.TabIndex = 7;
			this.todoRadioButton.TabStop = true;
			this.todoRadioButton.Text = "Todo";
			this.todoRadioButton.UseVisualStyleBackColor = true;
			this.todoRadioButton.CheckedChanged += new System.EventHandler(this.todoRadioButton_CheckedChanged);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(575, 18);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(96, 13);
			this.label12.TabIndex = 6;
			this.label12.Text = "Detalle de la orden";
			// 
			// detallesOrdenDataGridView
			// 
			this.detallesOrdenDataGridView.AllowUserToAddRows = false;
			this.detallesOrdenDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.detallesOrdenDataGridView.Location = new System.Drawing.Point(578, 34);
			this.detallesOrdenDataGridView.Name = "detallesOrdenDataGridView";
			this.detallesOrdenDataGridView.ReadOnly = true;
			this.detallesOrdenDataGridView.RowHeadersVisible = false;
			this.detallesOrdenDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.detallesOrdenDataGridView.Size = new System.Drawing.Size(601, 152);
			this.detallesOrdenDataGridView.TabIndex = 5;
			// 
			// ordenesDataGridView
			// 
			this.ordenesDataGridView.AllowUserToAddRows = false;
			this.ordenesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.ordenesDataGridView.Location = new System.Drawing.Point(6, 17);
			this.ordenesDataGridView.Name = "ordenesDataGridView";
			this.ordenesDataGridView.ReadOnly = true;
			this.ordenesDataGridView.RowHeadersVisible = false;
			this.ordenesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.ordenesDataGridView.Size = new System.Drawing.Size(549, 495);
			this.ordenesDataGridView.TabIndex = 3;
			this.ordenesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ordenesDataGridView_CellClick);
			// 
			// clientesTabPage
			// 
			this.clientesTabPage.Controls.Add(this.groupBox5);
			this.clientesTabPage.Controls.Add(this.groupBox4);
			this.clientesTabPage.Controls.Add(this.claveTextBox);
			this.clientesTabPage.Controls.Add(this.label21);
			this.clientesTabPage.Controls.Add(this.usuarioTextBox);
			this.clientesTabPage.Controls.Add(this.label20);
			this.clientesTabPage.Controls.Add(this.guardarCliente);
			this.clientesTabPage.Controls.Add(this.eMailTextBox);
			this.clientesTabPage.Controls.Add(this.label19);
			this.clientesTabPage.Controls.Add(this.apellidoTextBox);
			this.clientesTabPage.Controls.Add(this.label15);
			this.clientesTabPage.Controls.Add(this.nombreTextBox);
			this.clientesTabPage.Controls.Add(this.label14);
			this.clientesTabPage.Controls.Add(this.label13);
			this.clientesTabPage.Controls.Add(this.clientesDataGridView);
			this.clientesTabPage.Location = new System.Drawing.Point(4, 22);
			this.clientesTabPage.Name = "clientesTabPage";
			this.clientesTabPage.Size = new System.Drawing.Size(1427, 622);
			this.clientesTabPage.TabIndex = 2;
			this.clientesTabPage.Text = "Clientes";
			this.clientesTabPage.UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.companiaCustomerBillingTextBox);
			this.groupBox5.Controls.Add(this.label18);
			this.groupBox5.Controls.Add(this.codigoPostalCustomerBillingTextBox);
			this.groupBox5.Controls.Add(this.label30);
			this.groupBox5.Controls.Add(this.apellidoCustomerBillingTextBox);
			this.groupBox5.Controls.Add(this.label31);
			this.groupBox5.Controls.Add(this.direccionCustomerBillingTextBox);
			this.groupBox5.Controls.Add(this.label32);
			this.groupBox5.Controls.Add(this.nombreCustomerBillingTextBox);
			this.groupBox5.Controls.Add(this.label33);
			this.groupBox5.Location = new System.Drawing.Point(601, 192);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(545, 141);
			this.groupBox5.TabIndex = 50;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Billing";
			// 
			// companiaCustomerBillingTextBox
			// 
			this.companiaCustomerBillingTextBox.Location = new System.Drawing.Point(96, 48);
			this.companiaCustomerBillingTextBox.Name = "companiaCustomerBillingTextBox";
			this.companiaCustomerBillingTextBox.Size = new System.Drawing.Size(387, 20);
			this.companiaCustomerBillingTextBox.TabIndex = 53;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(41, 50);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(54, 13);
			this.label18.TabIndex = 52;
			this.label18.Text = "Compañia";
			// 
			// codigoPostalCustomerBillingTextBox
			// 
			this.codigoPostalCustomerBillingTextBox.Location = new System.Drawing.Point(96, 109);
			this.codigoPostalCustomerBillingTextBox.Name = "codigoPostalCustomerBillingTextBox";
			this.codigoPostalCustomerBillingTextBox.Size = new System.Drawing.Size(147, 20);
			this.codigoPostalCustomerBillingTextBox.TabIndex = 49;
			this.codigoPostalCustomerBillingTextBox.TextChanged += new System.EventHandler(this.codigoPostalCustomerBillingTextBox_TextChanged);
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Location = new System.Drawing.Point(19, 112);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(71, 13);
			this.label30.TabIndex = 48;
			this.label30.Text = "Código postal";
			this.label30.Click += new System.EventHandler(this.label30_Click);
			// 
			// apellidoCustomerBillingTextBox
			// 
			this.apellidoCustomerBillingTextBox.Location = new System.Drawing.Point(325, 22);
			this.apellidoCustomerBillingTextBox.Name = "apellidoCustomerBillingTextBox";
			this.apellidoCustomerBillingTextBox.Size = new System.Drawing.Size(202, 20);
			this.apellidoCustomerBillingTextBox.TabIndex = 47;
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Location = new System.Drawing.Point(267, 25);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(44, 13);
			this.label31.TabIndex = 46;
			this.label31.Text = "Apellido";
			// 
			// direccionCustomerBillingTextBox
			// 
			this.direccionCustomerBillingTextBox.Location = new System.Drawing.Point(96, 77);
			this.direccionCustomerBillingTextBox.Name = "direccionCustomerBillingTextBox";
			this.direccionCustomerBillingTextBox.Size = new System.Drawing.Size(387, 20);
			this.direccionCustomerBillingTextBox.TabIndex = 47;
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Location = new System.Drawing.Point(38, 77);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(52, 13);
			this.label32.TabIndex = 46;
			this.label32.Text = "Dirección";
			// 
			// nombreCustomerBillingTextBox
			// 
			this.nombreCustomerBillingTextBox.Location = new System.Drawing.Point(96, 22);
			this.nombreCustomerBillingTextBox.Name = "nombreCustomerBillingTextBox";
			this.nombreCustomerBillingTextBox.Size = new System.Drawing.Size(165, 20);
			this.nombreCustomerBillingTextBox.TabIndex = 47;
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Location = new System.Drawing.Point(46, 25);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(44, 13);
			this.label33.TabIndex = 46;
			this.label33.Text = "Nombre";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.companiaCustomerShippingTextBox);
			this.groupBox4.Controls.Add(this.label38);
			this.groupBox4.Controls.Add(this.CodigoPostalCustomerShippingTextBox);
			this.groupBox4.Controls.Add(this.label26);
			this.groupBox4.Controls.Add(this.apellidoCustomerShippingTextBox);
			this.groupBox4.Controls.Add(this.label27);
			this.groupBox4.Controls.Add(this.direccionCustomerShippingTextBox);
			this.groupBox4.Controls.Add(this.label28);
			this.groupBox4.Controls.Add(this.nombreCustomerShippingTextBox);
			this.groupBox4.Controls.Add(this.label29);
			this.groupBox4.Location = new System.Drawing.Point(601, 357);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(545, 150);
			this.groupBox4.TabIndex = 47;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Shipping";
			// 
			// companiaCustomerShippingTextBox
			// 
			this.companiaCustomerShippingTextBox.Location = new System.Drawing.Point(96, 48);
			this.companiaCustomerShippingTextBox.Name = "companiaCustomerShippingTextBox";
			this.companiaCustomerShippingTextBox.Size = new System.Drawing.Size(387, 20);
			this.companiaCustomerShippingTextBox.TabIndex = 55;
			// 
			// label38
			// 
			this.label38.AutoSize = true;
			this.label38.Location = new System.Drawing.Point(41, 50);
			this.label38.Name = "label38";
			this.label38.Size = new System.Drawing.Size(54, 13);
			this.label38.TabIndex = 54;
			this.label38.Text = "Compañia";
			// 
			// CodigoPostalCustomerShippingTextBox
			// 
			this.CodigoPostalCustomerShippingTextBox.Location = new System.Drawing.Point(96, 111);
			this.CodigoPostalCustomerShippingTextBox.Name = "CodigoPostalCustomerShippingTextBox";
			this.CodigoPostalCustomerShippingTextBox.Size = new System.Drawing.Size(147, 20);
			this.CodigoPostalCustomerShippingTextBox.TabIndex = 49;
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(19, 114);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(71, 13);
			this.label26.TabIndex = 48;
			this.label26.Text = "Código postal";
			// 
			// apellidoCustomerShippingTextBox
			// 
			this.apellidoCustomerShippingTextBox.Location = new System.Drawing.Point(325, 22);
			this.apellidoCustomerShippingTextBox.Name = "apellidoCustomerShippingTextBox";
			this.apellidoCustomerShippingTextBox.Size = new System.Drawing.Size(202, 20);
			this.apellidoCustomerShippingTextBox.TabIndex = 47;
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(275, 25);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(44, 13);
			this.label27.TabIndex = 46;
			this.label27.Text = "Apellido";
			// 
			// direccionCustomerShippingTextBox
			// 
			this.direccionCustomerShippingTextBox.Location = new System.Drawing.Point(96, 79);
			this.direccionCustomerShippingTextBox.Name = "direccionCustomerShippingTextBox";
			this.direccionCustomerShippingTextBox.Size = new System.Drawing.Size(430, 20);
			this.direccionCustomerShippingTextBox.TabIndex = 47;
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(38, 79);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(52, 13);
			this.label28.TabIndex = 46;
			this.label28.Text = "Dirección";
			// 
			// nombreCustomerShippingTextBox
			// 
			this.nombreCustomerShippingTextBox.Location = new System.Drawing.Point(96, 22);
			this.nombreCustomerShippingTextBox.Name = "nombreCustomerShippingTextBox";
			this.nombreCustomerShippingTextBox.Size = new System.Drawing.Size(165, 20);
			this.nombreCustomerShippingTextBox.TabIndex = 47;
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Location = new System.Drawing.Point(46, 25);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(44, 13);
			this.label29.TabIndex = 46;
			this.label29.Text = "Nombre";
			// 
			// claveTextBox
			// 
			this.claveTextBox.Location = new System.Drawing.Point(926, 114);
			this.claveTextBox.Name = "claveTextBox";
			this.claveTextBox.Size = new System.Drawing.Size(201, 20);
			this.claveTextBox.TabIndex = 44;
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(886, 121);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(34, 13);
			this.label21.TabIndex = 43;
			this.label21.Text = "Clave";
			// 
			// usuarioTextBox
			// 
			this.usuarioTextBox.Location = new System.Drawing.Point(655, 114);
			this.usuarioTextBox.Name = "usuarioTextBox";
			this.usuarioTextBox.Size = new System.Drawing.Size(166, 20);
			this.usuarioTextBox.TabIndex = 42;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(598, 117);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(43, 13);
			this.label20.TabIndex = 41;
			this.label20.Text = "Usuario";
			// 
			// guardarCliente
			// 
			this.guardarCliente.Location = new System.Drawing.Point(1043, 527);
			this.guardarCliente.Name = "guardarCliente";
			this.guardarCliente.Size = new System.Drawing.Size(126, 23);
			this.guardarCliente.TabIndex = 40;
			this.guardarCliente.Text = "Guardar Cliente";
			this.guardarCliente.UseVisualStyleBackColor = true;
			// 
			// eMailTextBox
			// 
			this.eMailTextBox.Location = new System.Drawing.Point(697, 78);
			this.eMailTextBox.Name = "eMailTextBox";
			this.eMailTextBox.Size = new System.Drawing.Size(201, 20);
			this.eMailTextBox.TabIndex = 39;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(598, 78);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(93, 13);
			this.label19.TabIndex = 38;
			this.label19.Text = "Correo electrónico";
			// 
			// apellidoTextBox
			// 
			this.apellidoTextBox.Location = new System.Drawing.Point(925, 38);
			this.apellidoTextBox.Name = "apellidoTextBox";
			this.apellidoTextBox.Size = new System.Drawing.Size(202, 20);
			this.apellidoTextBox.TabIndex = 31;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(875, 44);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(44, 13);
			this.label15.TabIndex = 30;
			this.label15.Text = "Apellido";
			// 
			// nombreTextBox
			// 
			this.nombreTextBox.Location = new System.Drawing.Point(655, 41);
			this.nombreTextBox.Name = "nombreTextBox";
			this.nombreTextBox.Size = new System.Drawing.Size(214, 20);
			this.nombreTextBox.TabIndex = 29;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(598, 41);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(44, 13);
			this.label14.TabIndex = 28;
			this.label14.Text = "Nombre";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(598, 19);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(39, 13);
			this.label13.TabIndex = 27;
			this.label13.Text = "Cliente";
			// 
			// clientesDataGridView
			// 
			this.clientesDataGridView.AllowUserToAddRows = false;
			this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.clientesDataGridView.Location = new System.Drawing.Point(13, 15);
			this.clientesDataGridView.Name = "clientesDataGridView";
			this.clientesDataGridView.ReadOnly = true;
			this.clientesDataGridView.RowHeadersVisible = false;
			this.clientesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.clientesDataGridView.Size = new System.Drawing.Size(549, 535);
			this.clientesDataGridView.TabIndex = 4;
			this.clientesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.clientesDataGridView_CellClick);
			// 
			// atributosTabPage
			// 
			this.atributosTabPage.Controls.Add(this.groupBox7);
			this.atributosTabPage.Controls.Add(this.atributosDataGridView);
			this.atributosTabPage.Controls.Add(this.button1);
			this.atributosTabPage.Location = new System.Drawing.Point(4, 22);
			this.atributosTabPage.Name = "atributosTabPage";
			this.atributosTabPage.Size = new System.Drawing.Size(1427, 622);
			this.atributosTabPage.TabIndex = 3;
			this.atributosTabPage.Text = "Atributos";
			this.atributosTabPage.UseVisualStyleBackColor = true;
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.eliminarAtributoBbutton);
			this.groupBox7.Controls.Add(this.groupBox8);
			this.groupBox7.Controls.Add(this.guardarAtributoButton);
			this.groupBox7.Controls.Add(this.NombreDelAtributo_TextBox);
			this.groupBox7.Controls.Add(this.label40);
			this.groupBox7.Location = new System.Drawing.Point(362, 36);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(427, 483);
			this.groupBox7.TabIndex = 11;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Atributo";
			// 
			// eliminarAtributoBbutton
			// 
			this.eliminarAtributoBbutton.Location = new System.Drawing.Point(258, 82);
			this.eliminarAtributoBbutton.Name = "eliminarAtributoBbutton";
			this.eliminarAtributoBbutton.Size = new System.Drawing.Size(136, 23);
			this.eliminarAtributoBbutton.TabIndex = 4;
			this.eliminarAtributoBbutton.Text = "Eliminar atributo";
			this.eliminarAtributoBbutton.UseVisualStyleBackColor = true;
			this.eliminarAtributoBbutton.Click += new System.EventHandler(this.eliminarAtributoBbutton_Click);
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.eliminarTerminoButton);
			this.groupBox8.Controls.Add(this.button2);
			this.groupBox8.Controls.Add(this.nombreDelTerminoTextBox);
			this.groupBox8.Controls.Add(this.label41);
			this.groupBox8.Controls.Add(this.terminosDataGridView);
			this.groupBox8.Location = new System.Drawing.Point(20, 146);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(389, 331);
			this.groupBox8.TabIndex = 3;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Términos";
			// 
			// eliminarTerminoButton
			// 
			this.eliminarTerminoButton.Location = new System.Drawing.Point(238, 259);
			this.eliminarTerminoButton.Name = "eliminarTerminoButton";
			this.eliminarTerminoButton.Size = new System.Drawing.Size(136, 23);
			this.eliminarTerminoButton.TabIndex = 6;
			this.eliminarTerminoButton.Text = "Elimina término";
			this.eliminarTerminoButton.UseVisualStyleBackColor = true;
			this.eliminarTerminoButton.Click += new System.EventHandler(this.eliminarTerminoButton_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(15, 259);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(136, 23);
			this.button2.TabIndex = 4;
			this.button2.Text = "Guardar término";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// nombreDelTerminoTextBox
			// 
			this.nombreDelTerminoTextBox.Location = new System.Drawing.Point(15, 219);
			this.nombreDelTerminoTextBox.Name = "nombreDelTerminoTextBox";
			this.nombreDelTerminoTextBox.Size = new System.Drawing.Size(211, 20);
			this.nombreDelTerminoTextBox.TabIndex = 5;
			// 
			// label41
			// 
			this.label41.AutoSize = true;
			this.label41.Location = new System.Drawing.Point(12, 202);
			this.label41.Name = "label41";
			this.label41.Size = new System.Drawing.Size(100, 13);
			this.label41.TabIndex = 4;
			this.label41.Text = "Nombre del térmimo";
			// 
			// terminosDataGridView
			// 
			this.terminosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.terminosDataGridView.Location = new System.Drawing.Point(6, 19);
			this.terminosDataGridView.Name = "terminosDataGridView";
			this.terminosDataGridView.ReadOnly = true;
			this.terminosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.terminosDataGridView.Size = new System.Drawing.Size(377, 150);
			this.terminosDataGridView.TabIndex = 3;
			this.terminosDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.terminosDataGridView_CellClick);
			// 
			// guardarAtributoButton
			// 
			this.guardarAtributoButton.Location = new System.Drawing.Point(20, 82);
			this.guardarAtributoButton.Name = "guardarAtributoButton";
			this.guardarAtributoButton.Size = new System.Drawing.Size(136, 23);
			this.guardarAtributoButton.TabIndex = 2;
			this.guardarAtributoButton.Text = "Guardar atributo";
			this.guardarAtributoButton.UseVisualStyleBackColor = true;
			this.guardarAtributoButton.Click += new System.EventHandler(this.guardarAtributoButton_Click);
			// 
			// NombreDelAtributo_TextBox
			// 
			this.NombreDelAtributo_TextBox.Location = new System.Drawing.Point(20, 45);
			this.NombreDelAtributo_TextBox.Name = "NombreDelAtributo_TextBox";
			this.NombreDelAtributo_TextBox.Size = new System.Drawing.Size(211, 20);
			this.NombreDelAtributo_TextBox.TabIndex = 1;
			// 
			// label40
			// 
			this.label40.AutoSize = true;
			this.label40.Location = new System.Drawing.Point(17, 28);
			this.label40.Name = "label40";
			this.label40.Size = new System.Drawing.Size(99, 13);
			this.label40.TabIndex = 0;
			this.label40.Text = "Nombre del atributo";
			// 
			// atributosDataGridView
			// 
			this.atributosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.atributosDataGridView.Location = new System.Drawing.Point(14, 36);
			this.atributosDataGridView.Name = "atributosDataGridView";
			this.atributosDataGridView.ReadOnly = true;
			this.atributosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.atributosDataGridView.Size = new System.Drawing.Size(332, 483);
			this.atributosDataGridView.TabIndex = 10;
			this.atributosDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.atributosDataGridView_CellClick);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(14, 525);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(141, 23);
			this.button1.TabIndex = 9;
			this.button1.Text = "Cargar atributos";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// actualizarTodosLosProducto
			// 
			this.actualizarTodosLosProducto.Location = new System.Drawing.Point(18, 553);
			this.actualizarTodosLosProducto.Name = "actualizarTodosLosProducto";
			this.actualizarTodosLosProducto.Size = new System.Drawing.Size(141, 49);
			this.actualizarTodosLosProducto.TabIndex = 28;
			this.actualizarTodosLosProducto.Text = "Actualizar todos los productos";
			this.actualizarTodosLosProducto.UseVisualStyleBackColor = true;
			this.actualizarTodosLosProducto.Click += new System.EventHandler(this.actualizarTodosLosProducto_Click);
			// 
			// regsitrosLeidos_textBox
			// 
			this.regsitrosLeidos_textBox.Location = new System.Drawing.Point(165, 579);
			this.regsitrosLeidos_textBox.Name = "regsitrosLeidos_textBox";
			this.regsitrosLeidos_textBox.Size = new System.Drawing.Size(118, 20);
			this.regsitrosLeidos_textBox.TabIndex = 29;
			// 
			// label47
			// 
			this.label47.AutoSize = true;
			this.label47.Location = new System.Drawing.Point(165, 563);
			this.label47.Name = "label47";
			this.label47.Size = new System.Drawing.Size(81, 13);
			this.label47.TabIndex = 30;
			this.label47.Text = "Registros leidos";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1459, 698);
			this.Controls.Add(this.ordenesTabControl);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "WooCommerce";
			this.ordenesTabControl.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.Variante.ResumeLayout(false);
			this.Variante.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.atributosDelProductoDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.variantesDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProductos)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxProducto)).EndInit();
			this.OrdenesTabPage.ResumeLayout(false);
			this.OrdenesTabPage.PerformLayout();
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.detallesOrdenDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ordenesDataGridView)).EndInit();
			this.clientesTabPage.ResumeLayout(false);
			this.clientesTabPage.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
			this.atributosTabPage.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.groupBox7.PerformLayout();
			this.groupBox8.ResumeLayout(false);
			this.groupBox8.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.terminosDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.atributosDataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.TabControl ordenesTabControl;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage OrdenesTabPage;
		private System.Windows.Forms.Button buttonProductos;
		private System.Windows.Forms.DataGridViewTextBoxColumn colNombreProducto;
		private System.Windows.Forms.DataGridViewTextBoxColumn colID;
		private System.Windows.Forms.DataGridView dataGridViewProductos;
		private System.Windows.Forms.Button buttonActualizarProductos;
		private System.Windows.Forms.PictureBox pictureBoxProducto;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxNombreLargo;
		private System.Windows.Forms.CheckedListBox checkedListBoxCategorias;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxURLImagen;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBoxNombreCorto;
		private System.Windows.Forms.RichTextBox richTextBoxCaracteristicas;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxPrecioDeVenta;
		private System.Windows.Forms.CheckBox checkBoxPublicar;
		private System.Windows.Forms.CheckBox productoEnOfertaCheckBox;
		private System.Windows.Forms.DateTimePicker inicioOfertadateTimePicker;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker finalOfertaDateTimePicker;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox precioDeOfertaTextBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox manejaExistenciaCheckBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox ExistenciaTextBox;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox totalDeVentasTextBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.DataGridView ordenesDataGridView;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.DataGridView detallesOrdenDataGridView;
		private System.Windows.Forms.RadioButton porProcesarradioButton;
		private System.Windows.Forms.RadioButton todoRadioButton;
		private System.Windows.Forms.TextBox versionTextBox;
		private System.Windows.Forms.TabPage clientesTabPage;
		private System.Windows.Forms.DataGridView clientesDataGridView;
		private System.Windows.Forms.Button buttonEstablecerImagen;
		private System.Windows.Forms.TextBox claveTextBox;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox usuarioTextBox;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Button guardarCliente;
		private System.Windows.Forms.TextBox eMailTextBox;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox apellidoTextBox;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox nombreTextBox;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox codigoPostalOrderShipping_textBox;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox apellidoOrderShipping_textBox;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox direccionOrderShipping_textBox;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox nombreOrderShipping_textBox;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox CodigoPostalCustomerShippingTextBox;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.TextBox apellidoCustomerShippingTextBox;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox direccionCustomerShippingTextBox;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.TextBox nombreCustomerShippingTextBox;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox codigoPostalCustomerBillingTextBox;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.TextBox apellidoCustomerBillingTextBox;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox direccionCustomerBillingTextBox;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TextBox nombreCustomerBillingTextBox;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.TextBox codigoPostalOrderBillingTextBox;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.TextBox apellidoOrderBillingTextBox;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.TextBox direccionOrderBillingTextBox;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.TextBox nombreOrderBillingtextBox;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox companiaOrderBillingtextBox;
		private System.Windows.Forms.TextBox companiaOrderShippingTextBox;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox companiaCustomerBillingTextBox;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox companiaCustomerShippingTextBox;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.TextBox costoDelEnvioTextBox;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.TabPage atributosTabPage;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.DataGridView atributosDataGridView;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.TextBox NombreDelAtributo_TextBox;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.Button guardarAtributoButton;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.DataGridView terminosDataGridView;
		private System.Windows.Forms.TextBox nombreDelTerminoTextBox;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button eliminarTerminoButton;
		private System.Windows.Forms.Button eliminarAtributoBbutton;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.CheckedListBox atributorCheckedListBox;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.DataGridView variantesDataGridView;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.DataGridView atributosDelProductoDataGridView;
		private System.Windows.Forms.Button eliminarAtributoButton;
		private System.Windows.Forms.Button eliminarVarianteButton;
		private System.Windows.Forms.GroupBox Variante;
		private System.Windows.Forms.Button agregarVarianteButton;
		private System.Windows.Forms.TextBox precioVarianteTextBox;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.RadioButton variableRadioButton;
		private System.Windows.Forms.RadioButton simpleRadioButton;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.CheckedListBox terminosCheckedListBox;
		private System.Windows.Forms.Button actualizarExistenciaButton;
		private System.Windows.Forms.Button actualizarTodosLosProducto;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.TextBox regsitrosLeidos_textBox;
	}
}

