﻿using System;
using System.Net.Http;
using System.Net;

using System.IO;

namespace WooCommerceMy
{
    public class Util
    {

        public static bool EsValidaLaImagen(string URLImagen)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(URLImagen);
                response = (HttpWebResponse)request.GetResponse();

                Stream StreamImagen = response.GetResponseStream();
                return true;
            }
            catch (HttpRequestException e)
            {
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (request != null) request.Abort();
                if (response != null) response.Close();
            }
        }
    }
}
