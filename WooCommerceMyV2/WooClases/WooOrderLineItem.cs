﻿
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooOrderLineItem
	{
		public WooOrderLineItem( ) {
			ID = 0;
			NOMBREDELPRODUCTO = "";
			CANTIDAD = 0;
			SUBTOTAL = 0;
			IMPUESTOSUBTOTAL = 0;
			TOTAL = 0;
			PRECIO = 0;

			oOrderLineItem = null;
		}

		public int ID { get; set; }

		public string NOMBREDELPRODUCTO { get; set; }

		public decimal CANTIDAD { get; set; }

		public decimal SUBTOTAL { get; set; }

		public decimal IMPUESTOSUBTOTAL { get; set; }

		public decimal TOTAL { get; set; }

		public decimal PRECIO { get; set; }

		public OrderLineItem oOrderLineItem;

	}
}
