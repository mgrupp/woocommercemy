﻿using System;
using System.Collections.Generic;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy
{
    public class WooProduct
	{
		public WooProduct()
		{
			ID = 0;
			NOMBRELARGO = "";
			NOMBRECORTO = "";
			CARACTERISTICAS = "";
			PRECIODEVENTA = 0;
			URLIMAGEN = "";
			PUBLICAR = false;
			PRODUCTOENOFERTA = false;
			INICIOOFERTA = DateTime.Now.Date;
			FINALOFERTA = DateTime.Now.Date;
			PRECIODEOFERTA = 0;
			MANEJAEXISTENCIA = false;
			EXISTENCIA = 0;
			TOTALDEVENTAS = 0;

			oProduct = new Product();
			oCategoriasProducto = new List<WooProductCategory>();
		}

		public int ID { get; set; }

		public string NOMBRELARGO { get; set; }

		public string NOMBRECORTO { get; set; }

		public string CARACTERISTICAS { get; set; }

		public decimal PRECIODEVENTA { get; set; }

		public string URLIMAGEN { get; set; }

		public bool PUBLICAR { get; set; }

		public bool PRODUCTOENOFERTA { get; set; }

		/// <summary>
		/// Fecha en que inicia la oferta
		/// </summary>
		public DateTime INICIOOFERTA { get; set; }

		/// <summary>
		/// Fecha en la que culmina la oferta
		/// </summary>
		public DateTime FINALOFERTA { get; set; } 

		public decimal PRECIODEOFERTA { get; set; }

		public bool MANEJAEXISTENCIA{ get; set; }  

		public int EXISTENCIA { get; set; }     

		public long TOTALDEVENTAS { get; set; } 

		public List<ProductCategoryLine> oCategorias { get; set; }

		public List<WooProductCategory> oCategoriasProducto { get; set; }

		public Product oProduct { get; set; }

		public ProductImage oProductImage { get; set; }

		public void AgregarCategoria(int Id ) {
			oCategoriasProducto.Add( new WooProductCategory() { ID = Id });
		}
	}
}
