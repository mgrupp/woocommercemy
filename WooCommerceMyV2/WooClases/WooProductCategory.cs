﻿
namespace WooCommerceMy {

	public class WooProductCategory
	{
		public WooProductCategory() 
		{
			ID = 0;
			NOMBRECATEGORIA = "";
		}

		public int ID { get; set; }

		public string NOMBRECATEGORIA { get; set; }
	}
}
