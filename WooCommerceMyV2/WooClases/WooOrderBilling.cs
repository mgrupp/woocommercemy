﻿using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooOrderBilling 
	{
		public WooOrderBilling( ) {
			NOMBRE = "";
			APELLIDO = "";
			DIRECCION1 = "";
		}

		public string NOMBRE { get; set; }

		public string APELLIDO { get; set; }

		public string DIRECCION1 { get; set; }

		public OrderBilling oOrderBilling;
	}
}
