﻿// Documentacion
//	https://github.com/XiaoFaye/WooCommerce.NET
//	https://woocommerce.github.io/woocommerce-rest-api-docs/#product-properties

using System;
using System.Collections.Generic;

using WooCommerceNET;
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy
{
    public class MyWooCommerce
    {
        #region MyWooCommerce
        public MyWooCommerce( string Url, string key, string secret ) {
            oRestAPI = new RestAPI( Url + WooCommerceAPIVersion, key, secret );
            oWCObject = new WCObject( oRestAPI );
        }

        public static string Version = "2.1";

        private static string WooCommerceAPIVersion = "//wp-json/wc/v2/";

        private RestAPI oRestAPI { get; set; }

        private WCObject oWCObject { get; set; }
        #endregion

        #region Producto
        public BOResultadoWoo GrabarProducto( WooProduct oWooProducto ) 
        {
            BOResultadoWoo oBOResultadoWoo = new BOResultadoWoo();

            oWooProducto.oProduct.name = oWooProducto.NOMBRELARGO;
            oWooProducto.oProduct.short_description = oWooProducto.NOMBRECORTO;
            oWooProducto.oProduct.description = oWooProducto.CARACTERISTICAS;
            oWooProducto.oProduct.regular_price = oWooProducto.PRECIODEVENTA;
            oWooProducto.oProduct.status = oWooProducto.PUBLICAR ? "publish" : "draft";
            oWooProducto.oProduct.on_sale = oWooProducto.PRODUCTOENOFERTA;
            oWooProducto.oProduct.date_on_sale_from_gmt = oWooProducto.INICIOOFERTA;
            oWooProducto.oProduct.date_on_sale_to_gmt = oWooProducto.FINALOFERTA;
            oWooProducto.oProduct.sale_price = oWooProducto.PRECIODEOFERTA;
            oWooProducto.oProduct.manage_stock = oWooProducto.MANEJAEXISTENCIA;
            oWooProducto.oProduct.stock_quantity = oWooProducto.EXISTENCIA;
            oWooProducto.oProduct.total_sales = oWooProducto.TOTALDEVENTAS;

            if (oWooProducto.oProductImage != null && oWooProducto.oProductImage.src != oWooProducto.URLIMAGEN) {
                if (oWooProducto.oProductImage.id > 0)
                    oWooProducto.oProduct.images.Remove( oWooProducto.oProductImage );

                if (oWooProducto.URLIMAGEN.Length > 0) {
                    oWooProducto.oProductImage = new ProductImage() {
                        src = oWooProducto.URLIMAGEN,
                    };
                    oWooProducto.oProduct.images = new List<ProductImage>();
                    oWooProducto.oProduct.images.Insert( 0, oWooProducto.oProductImage );
                }
            }

            if (oWooProducto.oProduct.categories == null)
                oWooProducto.oProduct.categories = new List<ProductCategoryLine>();
            else
                oWooProducto.oProduct.categories.Clear();

            foreach (WooProductCategory categoriaProducto in oWooProducto.oCategoriasProducto)
                oWooProducto.oProduct.categories.Add( new ProductCategoryLine() { id = categoriaProducto.ID } );

            try {
                if (oWooProducto.ID == 0) {
                    oWooProducto.oProduct = oWCObject.Product.Add( oWooProducto.oProduct ).GetAwaiter().GetResult();
                    oWooProducto.ID = oWooProducto.oProduct.id.Value;
                } else {
                    oWooProducto.oProduct = (oWCObject.Product.Update( oWooProducto.ID, oWooProducto.oProduct, new Dictionary<string, string>() )).Result;
                }
                oBOResultadoWoo.Objeto = oWooProducto;
                oBOResultadoWoo.Exito = true;
                
            } catch (Exception e) {
                oBOResultadoWoo.Exito = false;
                oBOResultadoWoo.ListaMensajes.Add( e.Message );
            }
            return oBOResultadoWoo;
        }

        public BOResultadoWoo LeerProductoPorID( int id ) 
        {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                Product oProduct = (oWCObject.Product.Get( id )).Result;
                if (oProduct != null) {
                    oResultadoWoo.Objeto = ImportarProducto( oProduct );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaMensajes.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public  List<WooProduct> LeerTodosLosProductos()
        {
            List<Product> products = (oWCObject.Product.GetAll( new Dictionary<string, string>() )).Result;

            List<WooProduct> oProductos = new List<WooProduct>();
            if (products != null)
                products.ForEach( oProduct => oProductos.Add( ImportarProducto( oProduct ) ) );
            return oProductos;
        }

        public WooProduct ImportarProducto(Product oProduct)
        {
            WooProduct oWooProducto = new WooProduct();
            oWooProducto.oProduct = oProduct;
            oWooProducto.ID = oProduct.id.Value;
            oWooProducto.NOMBRELARGO = oProduct.name;
            oWooProducto.NOMBRECORTO = (oProduct.short_description.Replace("<p>", "")).Replace("</p>", ""); ;
            oWooProducto.CARACTERISTICAS = (oProduct.description.Replace("<p>", "")).Replace("</p>", "");
            oWooProducto.oCategorias = oProduct.categories;
            oWooProducto.PRECIODEVENTA = oProduct.regular_price.Value;
            oWooProducto.PUBLICAR = (oProduct.status == "publish");
            oWooProducto.PRODUCTOENOFERTA = oProduct.on_sale.Value;
            if (oProduct.date_on_sale_from_gmt != null && oProduct.date_on_sale_from_gmt != DateTime.MinValue)
                oWooProducto.INICIOOFERTA = oProduct.date_on_sale_from_gmt.Value;
            if (oProduct.date_on_sale_to_gmt != null && oProduct.date_on_sale_to_gmt != DateTime.MinValue)
                oWooProducto.FINALOFERTA = oProduct.date_on_sale_to_gmt.Value;
            if (oProduct.sale_price != null)
                oWooProducto.PRECIODEOFERTA = oProduct.sale_price.Value;
            oWooProducto.MANEJAEXISTENCIA = oProduct.manage_stock.Value;
            if (oProduct.stock_quantity != null)
                oWooProducto.EXISTENCIA = oProduct.stock_quantity.Value;
            if (oProduct.total_sales != null)
                oWooProducto.TOTALDEVENTAS = oProduct.total_sales.Value;

            oWooProducto.oCategoriasProducto = LeerTodasLasCategoriasDelProducto( oWooProducto );

            if (oProduct.images.Count > 0)
            {
                oWooProducto.oProductImage = oProduct.images[0];
                oWooProducto.URLIMAGEN = oWooProducto.oProductImage.src;
            }
            else
            {
                oWooProducto.oProductImage = new ProductImage();
            }
            return oWooProducto;
        }

        #endregion

        #region Categoria
        public BOResultadoWoo GrabarCategoria( WooCategory pCategoria ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            pCategoria.oProductCategory.name = pCategoria.NOMBRECATEGORIA;
            try {
                if (pCategoria.ID == 0) {
                    pCategoria.oProductCategory = oWCObject.Category.Add( pCategoria.oProductCategory ).GetAwaiter().GetResult();
                    pCategoria.ID = pCategoria.oProductCategory.id.Value;
                } else
                    pCategoria.oProductCategory = (oWCObject.Category.Update( pCategoria.ID, pCategoria.oProductCategory, new Dictionary<string, string>() )).Result;

                oResultadoWoo.Objeto = pCategoria;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaMensajes.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public WooCategory ImportarCategoria( ProductCategory oProductCategory )
        {
            WooCategory wooCategory = new WooCategory() 
            {
                oProductCategory = oProductCategory,
                ID = oProductCategory.id.Value,
                NOMBRECATEGORIA = oProductCategory.name,
                IDCATEGORIAPADRE = (oProductCategory.parent != null) ? oProductCategory.parent.Value : 0,
                MODOVISUALIZACION = oProductCategory.display,
            };
            return wooCategory;
        }

        public List<WooCategory> LeerTodasLasCategorias() {
            List<ProductCategory> ProductCategorys = (oWCObject.Category.GetAll( new Dictionary<string, string>() )).Result;

            List<WooCategory> oCategorias = new List<WooCategory>();
            if(ProductCategorys != null)
                ProductCategorys.ForEach( oProductCategory => oCategorias.Add( ImportarCategoria( oProductCategory ) ) );

            return oCategorias;
        }
        #endregion

        #region CategoriaProducto
        public WooProductCategory ImportarCategoriaProducto( ProductCategoryLine pProductCategoryLine ) 
        {
            WooProductCategory oCategoriaProducto = new WooProductCategory() {
                ID = pProductCategoryLine.id.Value,
                NOMBRECATEGORIA = pProductCategoryLine.name,
            };
            return oCategoriaProducto;
        }

        public List<WooProductCategory> LeerTodasLasCategoriasDelProducto(WooProduct oProducto) 
        {
            List<WooProductCategory> oCategoriasProducto = new List<WooProductCategory>();
            foreach (ProductCategoryLine productCategory in oProducto.oProduct.categories)
            {
                oCategoriasProducto.Add( ImportarCategoriaProducto( productCategory ) );
            }
            return oCategoriasProducto;
        }
        #endregion

        #region Cliente
        public BOResultadoWoo LeerClientePorID( int id ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                Customer oCustomer = (oWCObject.Customer.Get( id )).Result;
                if (oCustomer != null) {
                    oResultadoWoo.Objeto = ImportarCliente( oCustomer );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaMensajes.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public WooCustomer ImportarCliente( Customer oCustomer ) {
            WooCustomer oCliente = new WooCustomer() {
                ID = Convert.ToInt32(oCustomer.id.Value),
                NOMBRE = oCustomer.first_name,
                APELLIDO = oCustomer.last_name,
                NOMBRECOMERCIAL = (oCustomer.billing != null) ? oCustomer.billing.company : "",
                DIRECCION = (oCustomer.billing != null) ? oCustomer.billing.address_1 + " " + oCustomer.billing.address_2 : "",
                TELEFONO = (oCustomer.billing != null) ? oCustomer.billing.phone : "",
                EMAIL = oCustomer.email,
            };
            return oCliente;
        }

        public BOResultadoWoo GrabarCliente( WooCustomer oCliente ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            if (oCliente.oCustomer.billing == null)
                oCliente.oCustomer.billing = new CustomerBilling();

            oCliente.oCustomer.first_name = oCliente.NOMBRE;
            oCliente.oCustomer.last_name = oCliente.APELLIDO;
            oCliente.oCustomer.billing.company = oCliente.NOMBRECOMERCIAL;
            oCliente.oCustomer.billing.address_1 = oCliente.DIRECCION;
            oCliente.oCustomer.billing.phone = oCliente.TELEFONO;
            oCliente.oCustomer.email = oCliente.EMAIL;
            oCliente.oCustomer.username = oCliente.USUARIO;
            oCliente.oCustomer.password = oCliente.CLAVE;

            try {
                if (oCliente.ID == 0) {
                    oCliente.oCustomer = oWCObject.Customer.Add( oCliente.oCustomer ).GetAwaiter().GetResult();
                    oCliente.ID = Convert.ToInt32( oCliente.oCustomer.id.Value );
                } else
                    oCliente.oCustomer = oWCObject.Customer.Update( oCliente.ID, oCliente.oCustomer, new Dictionary<string, string>() ).GetAwaiter().GetResult();

                oResultadoWoo.Objeto = oCliente;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaMensajes.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public List<WooCustomer> LeerTodosLosClientes() {             
            List<Customer> oCustomer = (oWCObject.Customer.GetAll( new Dictionary<string, string>() )).Result;
            
            List<WooCustomer> oClientes = new List<WooCustomer>();
            if (oCustomer != null)
                oCustomer.ForEach( Customer => oClientes.Add( ImportarCliente( Customer ) ) );
            
            return oClientes;
        }
        #endregion

        #region Orden
        private WooOrder ImportarOrden( Order oOrder ) {
            WooOrder oOrden = new WooOrder()
            {
                ID = oOrder.id.Value,
                NUMEROORDEN = oOrder.number,
                ESTADO = oOrder.status,
                MONEDA = oOrder.currency,
                FECHADECREACION = (oOrder.date_created == null) ? DateTime.Now : oOrder.date_created.Value,
                DESCUENTOTOTAL = (oOrder.discount_total == null) ? 0 : oOrder.discount_total.Value,
                DESCUENTOIMPUESTO = (oOrder.discount_tax == null) ? 0 : oOrder.discount_tax.Value,
                TOTALENVIO = (oOrder.shipping_total == null) ? 0 : oOrder.shipping_total.Value,
                IMPUESTOENVIO = (oOrder.shipping_tax == null) ? 0 : oOrder.shipping_tax.Value,
                TOTAL = (oOrder.total == null) ? 0 : oOrder.total.Value,
                IMPUESTOTOTAL = (oOrder.total_tax == null) ? 0 : oOrder.total_tax.Value,
                IDCLIENTE = (oOrder.customer_id == null) ? 0 : oOrder.customer_id.Value,
                NOTACLIENTE = oOrder.customer_note,
            };

            oOrden.oOrder = oOrder;
            oOrden.oDatosDeFacturacion = ImportarDatosDeFacturacion( oOrder.billing );
            oOrden.oDetallesDeLaOrden =  ImportarDetallesDeLaOrden( oOrder );

            return oOrden;
        }

        public BOResultadoWoo LeerOrdenPorID( int id ) 
        {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                Order oOrder = (oWCObject.Order.Get( id )).Result;

                if (oOrder != null) {
                    oResultadoWoo.Objeto = ImportarOrden( oOrder );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaMensajes.Add( e.Message );
            }

            return oResultadoWoo;
        }

        public BOResultadoWoo GrabarOrden( WooOrder oOrden ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            oOrden.oOrder.id = oOrden.ID;
            oOrden.oOrder.number = oOrden.NUMEROORDEN;
            oOrden.oOrder.status = oOrden.ESTADO;
            oOrden.oOrder.currency = oOrden.MONEDA;
            oOrden.oOrder.date_created = oOrden.FECHADECREACION;
            oOrden.oOrder.discount_total = oOrden.DESCUENTOTOTAL;
            oOrden.oOrder.discount_tax = oOrden.DESCUENTOIMPUESTO;
            oOrden.oOrder.shipping_total = oOrden.TOTALENVIO;
            oOrden.oOrder.shipping_tax = oOrden.IMPUESTOENVIO;
            oOrden.oOrder.total = oOrden.TOTAL;
            oOrden.oOrder.total_tax = oOrden.IMPUESTOTOTAL;
            oOrden.oOrder.customer_id = oOrden.IDCLIENTE;
            oOrden.oOrder.customer_note = oOrden.NOTACLIENTE;

            try {
                if (oOrden.ID == 0) {
                    oOrden.oOrder = oWCObject.Order.Add( oOrden.oOrder ).GetAwaiter().GetResult();
                    oOrden.ID = oOrden.oOrder.id.Value;
                } else
                    oOrden.oOrder = oWCObject.Order.Update( oOrden.oOrder.id.Value, oOrden.oOrder, new Dictionary<string, string>() ).GetAwaiter().GetResult();

                oResultadoWoo.Objeto = oOrden;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaMensajes.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public List<WooOrder> LeerTodasLasOrdenes() 
        {
            //
            // Parametros del Dictionary	{ "include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" }
            //  wcObject.Product.GetAll( new Dictionary<string, string>() { {"include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" } })
            //
            List<Order> orders = (oWCObject.Order.GetAll( new Dictionary<string, string>() )).Result;

            List<WooOrder> oOrdenes = new List<WooOrder>();
            if (orders != null)
                    orders.ForEach( oOrder => oOrdenes.Add( ImportarOrden( oOrder ) ) );
            return oOrdenes;
        }

        public List<WooOrder> LeerOrdenesPendientes() 
        {
            //
            // Parametros del Dictionary	{ "include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" }
            //  wcObject.Product.GetAll( new Dictionary<string, string>() { {"include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" } })
            //
            List<Order> orders = (oWCObject.Order.GetAll( new Dictionary<string, string>() { { "status", WooStatusOrder.PROCESSING } } )).Result;

            List<WooOrder> oOrdenes = new List<WooOrder>();
            if (orders != null)
                orders.ForEach( oOrder => oOrdenes.Add( ImportarOrden( oOrder ) ) );

            return oOrdenes;
        }
		#endregion

		#region DatosDeFacturacion
		private WooOrderBilling ImportarDatosDeFacturacion( OrderBilling oOrderBilling ) {

           WooOrderBilling oDatosDeFacturacion = new WooOrderBilling() 
            {
                NOMBRE = oOrderBilling.first_name,
                APELLIDO = oOrderBilling.last_name,
                DIRECCION1 = oOrderBilling.address_1,
            };
            return oDatosDeFacturacion;
        }
		#endregion

		#region DetalleOrden
		public WooOrderLineItem ImportarDetalleOrden( OrderLineItem oOrderLineItem ) {
            WooOrderLineItem oDetalleOrden = new WooOrderLineItem();
            oDetalleOrden.ID = oOrderLineItem.id.Value;
            oDetalleOrden.NOMBREDELPRODUCTO = oOrderLineItem.name;
            oDetalleOrden.CANTIDAD = oOrderLineItem.quantity.Value;
            oDetalleOrden.SUBTOTAL = oOrderLineItem.subtotal.Value;
            oDetalleOrden.IMPUESTOSUBTOTAL = oOrderLineItem.subtotal_tax.Value;
            oDetalleOrden.TOTAL = oOrderLineItem.total.Value;
            oDetalleOrden.PRECIO = oOrderLineItem.price.Value;

            oDetalleOrden.oOrderLineItem = oOrderLineItem;

            return oDetalleOrden;
        }
        
        public List<WooOrderLineItem> ImportarDetallesDeLaOrden( Order order ) {
            List<WooOrderLineItem> oDetallesDeLaOrden = new List<WooOrderLineItem>();

            if(order.line_items != null) {
                order.line_items.ForEach( orderLineItem => oDetallesDeLaOrden.Add( ImportarDetalleOrden( orderLineItem ) ) );
            }
            return oDetallesDeLaOrden;
        }
        #endregion
    }
}
