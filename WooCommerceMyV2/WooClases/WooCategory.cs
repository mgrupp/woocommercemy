﻿
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy
{
	public class WooCategory 
	{
		public WooCategory( ) 
		{
			ID = 0;
			NOMBRECATEGORIA = "";
			IDCATEGORIAPADRE = 0;
			MODOVISUALIZACION = WooCategoryDisplay.DEFAULT;

			oProductCategory = new ProductCategory();
		}

		public int ID { get; set; }

		public string NOMBRECATEGORIA { get; set; }

		public int IDCATEGORIAPADRE { get; set; }

		public string MODOVISUALIZACION = WooCategoryDisplay.DEFAULT;

		public ProductCategory oProductCategory { get; set; }

		public WooCategory oCategoriaPadre { get; set; }

		public override string ToString()
		{
			return NOMBRECATEGORIA;
		}

		/*
		public Object Tag()
		{
			return this;
		}
		*/
	};
}
