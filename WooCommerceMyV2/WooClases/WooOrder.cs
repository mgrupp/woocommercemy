﻿using System;
using System.Collections.Generic;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooOrder
	{
		public WooOrder() 
		{
			ID = 0;
			NUMEROORDEN = "";
			ESTADO = WooStatusOrder.PENDING;
			MONEDA = "";               
			FECHADECREACION = DateTime.Now.Date;
			DESCUENTOTOTAL = 0;
			DESCUENTOIMPUESTO = 0;
			TOTALENVIO = 0;
			IMPUESTOENVIO = 0;
			TOTAL = 0;
			IMPUESTOTOTAL = 0;
			IDCLIENTE = 0;
			NOTACLIENTE = "";

			oDatosDeFacturacion = new WooOrderBilling();
			oDetallesDeLaOrden = new List<WooOrderLineItem>();
			oOrder = new Order();
		}

		public int ID { get; set; }

		public string NUMEROORDEN { get; set; }

		public string ESTADO { get; set; }

		public string MONEDA { get; set; }

		public DateTime FECHADECREACION { get; set; }

		public decimal DESCUENTOTOTAL { get; set; }

		public decimal DESCUENTOIMPUESTO { get; set; }

		public decimal TOTALENVIO { get; set; }

		public decimal IMPUESTOENVIO { get; set; }

		public decimal TOTAL { get; set; }

		public decimal IMPUESTOTOTAL { get; set; }

		public int IDCLIENTE { get; set; }

		public string NOTACLIENTE { get; set; }

		public WooOrderBilling oDatosDeFacturacion { get; set; }
		
		public List<WooOrderLineItem> oDetallesDeLaOrden { get; set; }
		
		public Order oOrder { get; set; }

	}
}
