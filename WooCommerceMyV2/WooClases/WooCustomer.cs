﻿using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooCustomer {

		public WooCustomer() {
			ID = 0;
			NOMBRE = "";
			APELLIDO = "";
			NOMBRECOMERCIAL = "";
			DIRECCION = "";
			TELEFONO = "";
			EMAIL = "";
			USUARIO = "";
			CLAVE = "";

			oCustomer = new Customer();
		}

		public int ID { get; set; }

		public string NOMBRE { get; set; }

		public string APELLIDO { get; set; }

		public string NOMBRECOMERCIAL { get; set; }

		public string DIRECCION { get; set; }

		public string TELEFONO { get; set; }

		public string EMAIL { get; set; }

		public string USUARIO { get; set; }

		public string CLAVE { get; set; }

		public Customer oCustomer;
	}
}
