﻿using System.Collections.Generic;

namespace WooCommerceMy
{
    public class BOResultadoWoo
    {
        public BOResultadoWoo()
        {
            Objeto = null;
            Exito = false;
            ListaErrores = new List<string>();
            ListaMensajes = new List<string>();
            oWooError = null;
        }

        public object Objeto { get; set; }

        public bool Exito { get; set; }

        public List<string> ListaErrores { get; set; }

        public string Errores
        {
            get
            {
                string errores = "";
                foreach (string error in this.ListaErrores)
                {
                    errores = errores + error + "\n";
                }
                return errores;
            }
            set
            {
            }
        }

        public List<string> ListaMensajes { get; set; }

        public string Mensajes
        {
            get
            {
                string mensajes = "";
                foreach (string mensaje in this.ListaMensajes)
                {
                    mensajes = mensajes + mensaje + "\n";
                }
                return mensajes;
            }
            set
            {
            }
        }

        public WooError oWooError { get; set; }
    }
}
