﻿// Documentacion
//	https://github.com/XiaoFaye/WooCommerce.NET
//	https://woocommerce.github.io/woocommerce-rest-api-docs/#product-properties
//  https://woocommerce.github.io/code-reference/classes/WC-Coupon.html
//  https://stripe.com/docs/api/products/list
//  https://github.com/XiaoFaye/WooCommerce.NET/issues/356

using System;
using System.Collections.Generic;

using Newtonsoft.Json;



using WooCommerceNET;

using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy
{
    public class MyWooCommerce
    {
        #region MyWooCommerce
        public MyWooCommerce(string Url, string key, string secret)
        {
            oRestAPI = new RestAPI(Url + WooCommerceAPIV3, key, secret);
            oWCObject = new WCObject(oRestAPI);
        }

        public static string Version = "3.53";

        private static string WooCommerceAPIV3 = "//wp-json/wc/v3/";

        private RestAPI oRestAPI { get; set; }

        public WCObject oWCObject { get; set; }
        #endregion

        #region PRODUCTO
        public BOResultadoWoo GrabarProducto(WooProduct oWooProducto)
        {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            oWooProducto.oProduct.name              = oWooProducto.WOONOMBRELARGO;
            oWooProducto.oProduct.short_description = oWooProducto.WOONOMBRECORTO;
            oWooProducto.oProduct.description       = oWooProducto.WOOCARACTERISTICAS;
            oWooProducto.oProduct.type              = oWooProducto.WOOTYPE;
            oWooProducto.oProduct.regular_price     = oWooProducto.WOOPRECIODEVENTA;
            oWooProducto.oProduct.status            = oWooProducto.WOOPUBLICAR ? "publish" : "draft";
            oWooProducto.oProduct.on_sale           = oWooProducto.WOOPRODUCTOENOFERTA;
            oWooProducto.oProduct.date_on_sale_from_gmt = oWooProducto.WOOINICIOOFERTA;
            oWooProducto.oProduct.date_on_sale_to_gmt   = oWooProducto.WOOFINALOFERTA;
            oWooProducto.oProduct.sale_price            = oWooProducto.WOOPRECIODEOFERTA;
            oWooProducto.oProduct.manage_stock          = oWooProducto.WOOMANEJAEXISTENCIA;
            oWooProducto.oProduct.stock_quantity        = oWooProducto.WOOEXISTENCIA;
            oWooProducto.oProduct.total_sales           = oWooProducto.WOOTOTALDEVENTAS;

            if (oWooProducto.oProductImage != null && oWooProducto.oProductImage.src != oWooProducto.WOOURLIMAGEN)
            {
                if (oWooProducto.oProductImage.id > 0)
                    oWooProducto.oProduct.images.Remove(oWooProducto.oProductImage);

                if (oWooProducto.WOOURLIMAGEN.Length > 0)
                {
                    oWooProducto.oProductImage = new ProductImage()
                    {
                        src = oWooProducto.WOOURLIMAGEN,
                    };
                    oWooProducto.oProduct.images = new List<ProductImage>();
                    oWooProducto.oProduct.images.Insert(0, oWooProducto.oProductImage);
                }
            }

            oWooProducto.oProduct.categories.Clear();
            oWooProducto.oCategoriasProducto.ForEach( categoriaProducto => oWooProducto.oProduct.categories.Add( new ProductCategoryLine() { id = categoriaProducto.WOOIDCATEGORIAPRODUCTO } ) );

            oWooProducto.oProduct.attributes.Clear();
            oWooProducto.oWooProductAttributeLines.ForEach( oWooProductAttributeLine => oWooProducto.oProduct.attributes.Add( oWooProductAttributeLine.oProductAttributeLine ) );

            try
            {
                if (oWooProducto.WOOIDPRODUCTO == 0)
                {
                    oWooProducto.oProduct = oWCObject.Product.Add(oWooProducto.oProduct).GetAwaiter().GetResult();

                    oWooProducto.WOOIDPRODUCTO = oWooProducto.oProduct.id.Value;
                }
                else
                {
                    oWooProducto.oProduct = (oWCObject.Product.Update(oWooProducto.WOOIDPRODUCTO, oWooProducto.oProduct, new Dictionary<string, string>())).Result;
                }
                oResultadoWoo.Objeto = oWooProducto;
                oResultadoWoo.Exito = true;
            }
            catch (Exception e)
            {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add(e.Message);
            }
            return oResultadoWoo;
        }

        public BOResultadoWoo LeerProductoPorID(int id)
        {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

			try {
                Product oProduct = (oWCObject.Product.Get( id )).Result;
                if (oProduct != null) {
                    oResultadoWoo.Objeto = ImportarProducto( oProduct );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public List<WooProduct> LeerTodosLosProductos(string search, string value)
        {
            Dictionary<string, string> oDictionary = new Dictionary<string, string>() { { search, value } };

            List<Product> products = (oWCObject.Product.GetAll( oDictionary )).Result;

            List<WooProduct> oProductos = new List<WooProduct>();
            if (products != null)
                products.ForEach(oProduct => oProductos.Add(ImportarProducto(oProduct)));
            return oProductos;
        }

        public WooProduct ImportarProducto(Product oProduct)
        {
            WooProduct oWooProducto = new WooProduct();
            oWooProducto.oProduct = oProduct;
            oWooProducto.WOOIDPRODUCTO = oProduct.id.Value;
            oWooProducto.WOONOMBRELARGO = oProduct.name;
            oWooProducto.WOONOMBRECORTO = (oProduct.short_description.Replace("<p>", "")).Replace("</p>", ""); ;
            oWooProducto.WOOTYPE = oProduct.type;
            oWooProducto.WOOCARACTERISTICAS = (oProduct.description.Replace("<p>", "")).Replace("</p>", "");
            oWooProducto.WOOPRECIODEVENTA = (oProduct.regular_price != null) ? oProduct.regular_price.Value : 0;
            oWooProducto.WOOPUBLICAR = (oProduct.status == "publish");
            oWooProducto.WOOPRODUCTOENOFERTA = oProduct.on_sale.Value;
            
            if (oProduct.date_on_sale_from_gmt != null && oProduct.date_on_sale_from_gmt != DateTime.MinValue)
                oWooProducto.WOOINICIOOFERTA = oProduct.date_on_sale_from_gmt.Value;
            if (oProduct.date_on_sale_to_gmt != null && oProduct.date_on_sale_to_gmt != DateTime.MinValue)
                oWooProducto.WOOFINALOFERTA = oProduct.date_on_sale_to_gmt.Value;
            if (oProduct.sale_price != null)
                oWooProducto.WOOPRECIODEOFERTA = oProduct.sale_price.Value;
            if (oProduct.manage_stock != null)
                oWooProducto.WOOMANEJAEXISTENCIA = oProduct.manage_stock.Value;
            if (oProduct.stock_quantity != null)
                oWooProducto.WOOEXISTENCIA = oProduct.stock_quantity.Value;
            if (oProduct.total_sales != null)
                oWooProducto.WOOTOTALDEVENTAS = oProduct.total_sales.Value;

            oWooProducto.oCategoriasProducto = LeerTodasLasCategoriasDelProducto(oWooProducto);

            if (oProduct.images.Count > 0)
            {
                oWooProducto.oProductImage = oProduct.images[0];
                oWooProducto.WOOURLIMAGEN = oWooProducto.oProductImage.src;
            }
            else
            {
                oWooProducto.oProductImage = new ProductImage();
            }

            if(oProduct.attributes.Count > 0) {
                oProduct.attributes.ForEach( oProductAttributeLine => oWooProducto.oWooProductAttributeLines.Add( ImportarLineaAtributoProducto( oProductAttributeLine ) ) );
            }

            if(oProduct.variations.Count > 0) {
                oProduct.variations.ForEach( idVariacion => oWooProducto.oVariaciones.Add( idVariacion ) );
            }
            return oWooProducto;
        }

        public WooProductAttributeLine ImportarLineaAtributoProducto( ProductAttributeLine oProductAttributeLine ) 
        {
            WooProductAttributeLine oWooProductAttributeLine = new WooProductAttributeLine();
            oWooProductAttributeLine.oProductAttributeLine = oProductAttributeLine;
            
            return oWooProductAttributeLine;
        }

        public void EliminarTodosLosProductAttributeLine( WooProduct oWooProduct ) {
            oWooProduct.oWooProductAttributeLines.Clear();
        }

        public void EliminarWooProductAttributeLine(WooProduct oWooProduct, int IdWooProductAttributeLine ) {
            oWooProduct.oWooProductAttributeLines.RemoveAll( oWooProductAttributeLine => oWooProductAttributeLine.WOOIDPRODUCTATTRIBUTELINE == IdWooProductAttributeLine );
		}
        
        public void EliminarWooProductAttributeLine( WooProduct oWooProduct, WooProductAttributeLine oWooProductAttributeLine ) {
            oWooProduct.oWooProductAttributeLines.Remove( oWooProductAttributeLine );
        }

        public void AgregarWooProductAttributeLine( WooProduct oWooProduct, int WooIdAttribute, List<WooProductAttributeTerm> oWooProductAttributeTerms ) {
            WooProductAttributeLine oWooProductAttributeLine;
            oWooProductAttributeLine = oWooProduct.oWooProductAttributeLines.Find( WooProductAttributeLine => WooProductAttributeLine.WOOIDPRODUCTATTRIBUTELINE == WooIdAttribute );
            if(oWooProductAttributeLine == null) {
                oWooProductAttributeLine = new WooProductAttributeLine();
                oWooProductAttributeLine.WOOIDPRODUCTATTRIBUTELINE = WooIdAttribute;

                foreach (WooProductAttributeTerm oWooAttributeTerm in oWooProductAttributeTerms) {
                    AgregarWooProductAttributeLineTerm( oWooProductAttributeLine, oWooAttributeTerm.WOONOMBREDELTERMINO );
                }
                oWooProduct.oWooProductAttributeLines.Add( oWooProductAttributeLine );
            }
        }

        public WooProductAttributeLine AgregarWooProductAttributeLine( WooProduct oWooProduct, int WooIdAttribute ) {
            WooProductAttributeLine oWooProductAttributeLine;
            oWooProductAttributeLine = oWooProduct.oWooProductAttributeLines.Find( WooProductAttributeLine => WooProductAttributeLine.WOOIDPRODUCTATTRIBUTELINE == WooIdAttribute );
            if (oWooProductAttributeLine == null) {
                oWooProductAttributeLine = new WooProductAttributeLine();
                oWooProductAttributeLine.WOOIDPRODUCTATTRIBUTELINE = WooIdAttribute;
                oWooProduct.oWooProductAttributeLines.Add( oWooProductAttributeLine );
            }
           
            return oWooProductAttributeLine;
        }

        public void AgregarWooProductAttributeLineTerm( WooProductAttributeLine oWooProductAttributeLine,  string WOONOMBREDELTERMINO ) {
            oWooProductAttributeLine.Terminos.Add( WOONOMBREDELTERMINO );
        }

        public void EliminarWooProductAttributeLineTerm( WooProductAttributeLine oWooProductAttributeLine, string WOONOMBREDELTERMINO ) {
            oWooProductAttributeLine.Terminos.Remove( WOONOMBREDELTERMINO );
        }

        public BOResultadoWoo EstablecerImagenProduco( int id, string URL ) {
            BOResultadoWoo oBOResultadoWoo = LeerProductoPorID( id );

			if (oBOResultadoWoo.Exito) {
                WooProduct oWooProduct = (WooProduct)oBOResultadoWoo.Objeto;
                oWooProduct.WOOURLIMAGEN = URL;
                oBOResultadoWoo = GrabarProducto( oWooProduct );
            }
            return oBOResultadoWoo;
        }

        public WooProductAttributeLine BuscarWooProductAttributeLine( WooProduct oWooProduct, int WOOIDPRODUCTATTRIBUTELINE ) {
            WooProductAttributeLine oWooProductAttributeLine = oWooProduct.oWooProductAttributeLines.Find( WooProductAttributeLines => WooProductAttributeLines.WOOIDPRODUCTATTRIBUTELINE == WOOIDPRODUCTATTRIBUTELINE );
            return oWooProductAttributeLine;
        }

        public BOResultadoWoo ActualizarExistencia(int wooIdProduct, decimal cantidad) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            oWCObject.Product.UpdateWithNull( wooIdProduct, new { stock_quantity = cantidad } );

            return oResultadoWoo;
        }

        #endregion

        #region VARIACIONES

        public BOResultadoWoo GrabarVariacion(WooVariation oWooVariation,  int idProducto ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                if(oWooVariation.WOOID == 0)
                    oWooVariation.oVariation = oWCObject.Product.Variations.Add( oWooVariation.oVariation, idProducto, new Dictionary<string, string>()).Result;
                else
                    oWooVariation.oVariation = oWCObject.Product.Variations.Update( oWooVariation.WOOID, oWooVariation.oVariation, idProducto, new Dictionary<string, string>()).Result;
                oResultadoWoo.Objeto = oWooVariation;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public BOResultadoWoo EliminarVariacion( int idVariacion, int idProducto ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                oWCObject.Product.Variations.Delete( idVariacion, idProducto, true, new Dictionary<string, string>() );

                oResultadoWoo.Objeto = null;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public WooVariation LeerVariacionePorID( int idVariacion, int idProducto ) {
            Variation oVariation;
            WooVariation oWooVariation = null;

            oVariation = oWCObject.Product.Variations.Get( idVariacion, idProducto, new Dictionary<string, string>() ).Result;
            if (oVariation != null) 
            {
                oWooVariation = ImportarVariacion( oVariation );
            }

            return oWooVariation;
        }

        public List<WooVariation> LeerTodasLasVariacionesDeUnProducto(int idProducto) {
            List<WooVariation> WooVariations = new List<WooVariation>();

            List<Variation> oVariations = oWCObject.Product.Variations.GetAll( idProducto, new Dictionary<string, string>() ).Result;
            if(oVariations != null)
                oVariations.ForEach( oVariation => WooVariations.Add( ImportarVariacion( oVariation ) ) );

            return WooVariations;
        }

        public WooVariation ImportarVariacion( Variation oVariation ) {
            WooVariation oWooVariation = new WooVariation();
            oWooVariation.oVariation = oVariation;

            return oWooVariation;
        }
        #endregion

        #region ATRIBUTOS
        public BOResultadoWoo GrabarAtributoDeProducto( WooProductAttribute oWooProductAttribute ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                if (oWooProductAttribute.WOOIDATRIBUTO == 0) {
                    oWooProductAttribute.oProductAttribute = oWCObject.Attribute.Add( oWooProductAttribute.oProductAttribute ).GetAwaiter().GetResult();
                } else {
                    oWooProductAttribute.oProductAttribute = oWCObject.Attribute.Update( oWooProductAttribute.WOOIDATRIBUTO, oWooProductAttribute.oProductAttribute, new Dictionary<string, string>() ).Result;
                }
                oResultadoWoo.Objeto = oWooProductAttribute;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
                try {
                    oResultadoWoo.oWooError = JsonConvert.DeserializeObject<WooError>( e.Message );
                } 
                catch { }
            }

            return oResultadoWoo;
        }

        public BOResultadoWoo EliminarAtributoDeProducto( int IDWooProductAttribute ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                oWCObject.Attribute.Delete( IDWooProductAttribute, true, new Dictionary<string, string>());
                oResultadoWoo.Objeto = null;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public BOResultadoWoo LeerAtributoDeProductoID( int id ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                ProductAttribute oProductAttribute = oWCObject.Attribute.Get( id, new Dictionary<string, string>()).Result;
                if (oProductAttribute != null) {
                    oResultadoWoo.Objeto = ImportarAtributoDeProducto( oProductAttribute );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
				try {
                    oResultadoWoo.oWooError = JsonConvert.DeserializeObject<WooError>( e.Message );
				} 
                catch { } 
            }
            return oResultadoWoo;
        }

        public List<WooProductAttribute> LeerTodosLosAtributos() {

            List<WooProductAttribute> oAtributos = new List<WooProductAttribute>();

            List<ProductAttribute> attributes = (oWCObject.Attribute.GetAll( new Dictionary<string, string>() { { "per_page", "100" } } )).Result;
            if(attributes != null) {
                attributes.ForEach( oProductAttribute => oAtributos.Add( ImportarAtributoDeProducto( oProductAttribute ) ) );
            }
            return oAtributos;
        }

        public WooProductAttribute ImportarAtributoDeProducto( ProductAttribute oProductAttribute ) {

            WooProductAttribute oWooProductAttribute = new WooProductAttribute();
            oWooProductAttribute.oProductAttribute = oProductAttribute;

            return oWooProductAttribute;
        }
        #endregion

        #region TERMINOS
        public BOResultadoWoo GrabarTerminoDelAtributo( WooProductAttributeTerm oWooAttributeTerm, int oIDProductAttribute ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                if (oWooAttributeTerm.WOOIDPRODUCATTRIBUTETERM == 0) {
                    oWooAttributeTerm.oProductAttributeTerm = oWCObject.Attribute.Terms.Add( oWooAttributeTerm.oProductAttributeTerm, oIDProductAttribute, new Dictionary<string, string>()).Result;
                } else {
                    oWooAttributeTerm.oProductAttributeTerm = 
                        oWCObject.Attribute.Terms.Update(oWooAttributeTerm.WOOIDPRODUCATTRIBUTETERM, oWooAttributeTerm.oProductAttributeTerm, oIDProductAttribute, new Dictionary<string, string>() ).Result;
                }
                oResultadoWoo.Objeto = oWooAttributeTerm;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }

            return oResultadoWoo;
        }

        public BOResultadoWoo EliminarTerminoDelAtributo( int IDWooAttributeTerm, int oIDProductAttribute ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                string resultado = oWCObject.Attribute.Terms.Delete( IDWooAttributeTerm, oIDProductAttribute, true, new Dictionary<string, string>() ).Result;

                oResultadoWoo.Objeto = null;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }

            return oResultadoWoo;
        }

        public BOResultadoWoo LeerTerminosDelAtributoPorID( int oIDProductAttributeTerm, int oIDProductAttribute ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                ProductAttributeTerm oProductAttributeTerm = oWCObject.Attribute.Terms.Get( oIDProductAttributeTerm, oIDProductAttribute, new Dictionary<string, string>() ).Result;
                if (oProductAttributeTerm != null) {
                    oResultadoWoo.Objeto = ImportarTerminoDelAtributo( oProductAttributeTerm );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }

            return oResultadoWoo;
        }

        public List<WooProductAttributeTerm> LeerTodosLosTerminosDelAtributo( int oIDProductAttribute ) {
            List<WooProductAttributeTerm> oWooProductAttributeTerms = new List<WooProductAttributeTerm>();

            List<ProductAttributeTerm> oTerms = oWCObject.Attribute.Terms.GetAll( oIDProductAttribute, new Dictionary<string, string>() ).Result;
            if (oTerms != null) {
                oTerms.ForEach( oTerm => oWooProductAttributeTerms.Add( ImportarTerminoDelAtributo( oTerm ) ) );
            }

            return oWooProductAttributeTerms;
        }

        public WooProductAttributeTerm ImportarTerminoDelAtributo( ProductAttributeTerm oProductAttributeTerm ) {

            WooProductAttributeTerm oWooAttributeTerm = new WooProductAttributeTerm();
            oWooAttributeTerm.oProductAttributeTerm = oProductAttributeTerm;

            return oWooAttributeTerm;
        }
        #endregion

        #region CATEGORIA

        public BOResultadoWoo GrabarCategoria(WooCategory pCategoria)
        {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            pCategoria.oProductCategory.name = pCategoria.WOONOMBRECATEGORIA;
            pCategoria.oProductCategory.parent = pCategoria.WOOIDCATEGORIAPADRE;
            try
            {
                if (pCategoria.WOOIDCATEGORIA == 0) {
                    pCategoria.oProductCategory = oWCObject.Category.Add( pCategoria.oProductCategory ).GetAwaiter().GetResult();
                    pCategoria.WOOIDCATEGORIA = pCategoria.oProductCategory.id.Value;
                } else
                    pCategoria.oProductCategory = (oWCObject.Category.Update( pCategoria.WOOIDCATEGORIA, pCategoria.oProductCategory, new Dictionary<string, string>())).Result;

                oResultadoWoo.Objeto = pCategoria;
                oResultadoWoo.Exito = true;
            }
            catch (Exception e)
            {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add(e.Message);
            }
            return oResultadoWoo;
        }

        public void EliminarCategoria( int wooIDCategoria )
        {
            oWCObject.Category.Delete( wooIDCategoria, true, new Dictionary<string, string>() );
        }

        public List<WooCategory> LeerTodasLasCategorias()
        {
            List<ProductCategory> ProductCategorys = oWCObject.Category.GetAll(new Dictionary<string, string>()).Result;

            List<WooCategory> oCategorias = new List<WooCategory>();
            if (ProductCategorys != null)
                ProductCategorys.ForEach(oProductCategory => oCategorias.Add(ImportarCategoria(oProductCategory)));

            return oCategorias;
        }

        public BOResultadoWoo LeerCategoriaID( int id ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                ProductCategory oProductCategory = oWCObject.Category.Get( id, new Dictionary<string,string>() ).Result;
                if(oProductCategory != null) {
                    oResultadoWoo.Objeto = ImportarCategoria( oProductCategory );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
                /*
                try {
                    oResultadoWoo.oWooError = JsonConvert.DeserializeObject<WooError>( e.Message );
                } catch { }
                */
            }
            return oResultadoWoo;
        }


        public WooCategory ImportarCategoria( ProductCategory oProductCategory ) {
            WooCategory wooCategory = new WooCategory() {
                oProductCategory = oProductCategory,
                WOOIDCATEGORIA = oProductCategory.id.Value,
                WOONOMBRECATEGORIA = oProductCategory.name,
                WOOIDCATEGORIAPADRE = (oProductCategory.parent != null) ? oProductCategory.parent.Value : 0,
                WOOMODOVISUALIZACION = oProductCategory.display,
            };
            return wooCategory;
        }
        #endregion

        #region CATEGORIA PRODUCTO
        public List<WooProductCategory> LeerTodasLasCategoriasDelProducto(WooProduct oProducto)
        {
            List<WooProductCategory> oCategoriasProducto = new List<WooProductCategory>();
            foreach (ProductCategoryLine productCategoryLine in oProducto.oProduct.categories)
            {
                oCategoriasProducto.Add(ImportarCategoriaProducto( productCategoryLine ) );
            }
            return oCategoriasProducto;
        }

        public WooProductCategory ImportarCategoriaProducto( ProductCategoryLine pProductCategoryLine ) {
            WooProductCategory oCategoriaProducto = new WooProductCategory() {
                WOOIDCATEGORIAPRODUCTO = pProductCategoryLine.id.Value,
                WOONOMBRECATEGORIA = pProductCategoryLine.name,
            };
            return oCategoriaProducto;
        }
        #endregion

        #region CLIENTE
        public BOResultadoWoo GrabarCliente( WooCustomer oCliente ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            if (oCliente.oCustomer == null)
                oCliente.oCustomer = new Customer();

            if (oCliente.oCustomer.billing == null)
                oCliente.oCustomer.billing = new CustomerBilling();

            if (oCliente.oCustomer.shipping == null)
                oCliente.oCustomer.shipping = new CustomerShipping();

            oCliente.oCustomer.first_name = oCliente.WOONOMBRE;
            oCliente.oCustomer.last_name = oCliente.WOOAPELLIDO;
            oCliente.oCustomer.email = oCliente.WOOEMAIL;
            oCliente.oCustomer.username = oCliente.WOOUSUARIO;
            oCliente.oCustomer.password = oCliente.WOOCLAVE;

            oCliente.oCustomer.billing.first_name = oCliente.oWooCustomerBilling.WOONOMBRE;
            oCliente.oCustomer.billing.last_name = oCliente.oWooCustomerBilling.WOOAPELLIDO;
            oCliente.oCustomer.billing.company = oCliente.oWooCustomerBilling.WOOCOMPANIA;
            oCliente.oCustomer.billing.address_1 = oCliente.oWooCustomerBilling.WOODIRECCION;
            oCliente.oCustomer.billing.address_2 = "";
            oCliente.oCustomer.billing.postcode = oCliente.oWooCustomerBilling.WOOCODIGOPOSTAL;
            oCliente.oCustomer.billing.phone = oCliente.oWooCustomerBilling.WOOTELEFONO;

            oCliente.oCustomer.shipping.first_name = oCliente.oWooCustomerShipping.WOONOMBRE;
            oCliente.oCustomer.shipping.last_name = oCliente.oWooCustomerShipping.WOOAPELLIDO;
            oCliente.oCustomer.shipping.company = oCliente.oWooCustomerShipping.WOOCOMPANIA;
            oCliente.oCustomer.shipping.address_1 = oCliente.oWooCustomerShipping.WOODIRECCION;
            oCliente.oCustomer.shipping.address_2 = "";
            oCliente.oCustomer.shipping.postcode = oCliente.oWooCustomerShipping.WOOCODIGOPOSTAL;

            try {
                if (oCliente.WOOIDCLIENTE == 0) {
                    oCliente.oCustomer = oWCObject.Customer.Add( oCliente.oCustomer ).GetAwaiter().GetResult();
                    oCliente.WOOIDCLIENTE = Convert.ToInt32( oCliente.oCustomer.id.Value );
                } else
                    oCliente.oCustomer = oWCObject.Customer.Update( oCliente.WOOIDCLIENTE, oCliente.oCustomer, new Dictionary<string, string>() ).GetAwaiter().GetResult();

                oResultadoWoo.Objeto = oCliente;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public BOResultadoWoo LeerClientePorID(int id)
        {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try
            {
                Customer oCustomer = (oWCObject.Customer.Get(id)).Result;
                if (oCustomer != null) {
                    oResultadoWoo.Objeto = ImportarCliente( oCustomer );
                    oResultadoWoo.Exito = true;
                }else
                    oResultadoWoo.Exito = false;
            }
            catch (Exception e)
            {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;
        }

        public WooCustomer ImportarCliente(Customer oCustomer)
        {
            WooCustomer oCliente = new WooCustomer()
            {
                WOOIDCLIENTE = Convert.ToInt32(oCustomer.id.Value),
                WOONOMBRE   = oCustomer.first_name,
                WOOAPELLIDO = oCustomer.last_name,
                WOOEMAIL    = oCustomer.email,
                WOOUSUARIO  = oCustomer.username,
                WOOCLAVE    = oCustomer.password,
            };

            oCliente.oWooCustomerBilling = ImportarCustomerBilling( oCustomer.billing );
            oCliente.oWooCustomerShipping = ImportarCustomerShipping( oCustomer.shipping );

            return oCliente;
        }

        private WooCustomerBilling ImportarCustomerBilling( CustomerBilling oCustomerBilling ) {
            WooCustomerBilling oWooCustomerBilling = new WooCustomerBilling();

            oWooCustomerBilling.WOONOMBRE       = oCustomerBilling.first_name;
            oWooCustomerBilling.WOOAPELLIDO     = oCustomerBilling.last_name;
            oWooCustomerBilling.WOOCOMPANIA     = oCustomerBilling.company;
            oWooCustomerBilling.WOODIRECCION    = (oCustomerBilling.address_1 + " " + oCustomerBilling.address_2).Trim();
            oWooCustomerBilling.WOOCODIGOPOSTAL = oCustomerBilling.postcode;
            oWooCustomerBilling.WOOEMAIL        = oCustomerBilling.email;
            oWooCustomerBilling.WOOTELEFONO     = oCustomerBilling.phone;

            return oWooCustomerBilling;
        }

        private WooCustomerShipping ImportarCustomerShipping( CustomerShipping oCustomerShipping ) {
            WooCustomerShipping oWooCustomerShipping = new WooCustomerShipping();

            oWooCustomerShipping.WOONOMBRE          = oCustomerShipping.first_name;
            oWooCustomerShipping.WOOAPELLIDO        = oCustomerShipping.last_name;
            oWooCustomerShipping.WOOCOMPANIA        = oCustomerShipping.company;
            oWooCustomerShipping.WOODIRECCION       = (oCustomerShipping.address_1 + " " + oCustomerShipping.address_2).Trim();
            oWooCustomerShipping.WOOCODIGOPOSTAL    = oCustomerShipping.postcode;
            
            return oWooCustomerShipping;
        }

        public List<WooCustomer> LeerTodosLosClientes()
        {
            List<Customer> oCustomer = (oWCObject.Customer.GetAll(new Dictionary<string, string>())).Result;

            List<WooCustomer> oClientes = new List<WooCustomer>();
            if (oCustomer != null)
                oCustomer.ForEach(Customer => oClientes.Add(ImportarCliente(Customer)));

            return oClientes;
        }
        #endregion

        #region ORDEN
        public BOResultadoWoo GrabarOrden( WooOrder oOrden ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            oOrden.oOrder.id = oOrden.WOOIDORDEN;
            oOrden.oOrder.number = oOrden.WOONUMEROORDEN;
            oOrden.oOrder.status = oOrden.WOOESTADO;
            oOrden.oOrder.currency = oOrden.WOOMONEDA;
            oOrden.oOrder.date_created = oOrden.WOOFECHADECREACION;
            oOrden.oOrder.discount_total = oOrden.WOODESCUENTOTOTAL;
            oOrden.oOrder.discount_tax = oOrden.WOODESCUENTOIMPUESTO;
            oOrden.oOrder.shipping_total = oOrden.WOOCOSTOENVIO;
            oOrden.oOrder.shipping_tax = oOrden.WOOIMPUESTOENVIO;
            oOrden.oOrder.total = oOrden.WOOTOTAL;
            oOrden.oOrder.total_tax = oOrden.WOOIMPUESTOTOTAL;
            oOrden.oOrder.customer_id = oOrden.WOOIDCLIENTE;
            oOrden.oOrder.customer_note = oOrden.WOONOTACLIENTE;

            oOrden.oOrder.billing.first_name = oOrden.oWooOrderBilling.WOONOMBRE;
            oOrden.oOrder.billing.last_name = oOrden.oWooOrderBilling.WOOAPELLIDO;
            oOrden.oOrder.billing.company = oOrden.oWooOrderBilling.WOOCOMPANIA;
            oOrden.oOrder.billing.address_1 = oOrden.oWooOrderBilling.WOODIRECCION;
            oOrden.oOrder.billing.address_2 = "";
            oOrden.oOrder.billing.postcode = oOrden.oWooOrderBilling.WOOCODIGOPOSTAL;
            oOrden.oOrder.billing.email = oOrden.oWooOrderBilling.WOOEMAIL;
            oOrden.oOrder.billing.phone = oOrden.oWooOrderBilling.WOOTELEFONO;

            oOrden.oOrder.shipping.first_name = oOrden.oWooOrderShipping.WOONOMBRE;
            oOrden.oOrder.shipping.last_name = oOrden.oWooOrderShipping.WOOAPELLIDO;
            oOrden.oOrder.shipping.company = oOrden.oWooOrderShipping.WOOCOMPANIA;
            oOrden.oOrder.shipping.address_1 = oOrden.oWooOrderShipping.WOODIRECCION;
            oOrden.oOrder.shipping.address_2 = "";
            oOrden.oOrder.shipping.postcode = oOrden.oWooOrderShipping.WOOCODIGOPOSTAL;

            try {
                if (oOrden.WOOIDORDEN == 0) {
                    oOrden.oOrder = oWCObject.Order.Add( oOrden.oOrder ).GetAwaiter().GetResult();
                    oOrden.WOOIDORDEN = oOrden.oOrder.id.Value;
                } else
                    oOrden.oOrder = oWCObject.Order.Update( oOrden.oOrder.id.Value, oOrden.oOrder, new Dictionary<string, string>() ).GetAwaiter().GetResult();
                oResultadoWoo.Objeto = oOrden;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;

        }

        private WooOrder ImportarOrden(Order oOrder)
        {
            WooOrder oOrden = new WooOrder();

            oOrden.WOOIDORDEN       = oOrder.id.Value;
            oOrden.WOONUMEROORDEN   = oOrder.number;
            oOrden.WOOESTADO        = oOrder.status;
            oOrden.WOOMONEDA        = oOrder.currency;
            oOrden.WOOFECHADECREACION   = (oOrder.date_created == null) ? DateTime.Now : oOrder.date_created.Value;
            oOrden.WOODESCUENTOTOTAL    = (oOrder.discount_total == null) ? 0 : oOrder.discount_total.Value;
            oOrden.WOODESCUENTOIMPUESTO = (oOrder.discount_tax == null) ? 0 : oOrder.discount_tax.Value;
            oOrden.WOOCOSTOENVIO        = (oOrder.shipping_total == null) ? 0 : oOrder.shipping_total.Value;
            oOrden.WOOIMPUESTOENVIO     = (oOrder.shipping_tax == null) ? 0 : oOrder.shipping_tax.Value;
            oOrden.WOOTOTAL         = (oOrder.total == null) ? 0 : oOrder.total.Value;
            oOrden.WOOIMPUESTOTOTAL = (oOrder.total_tax == null) ? 0 : oOrder.total_tax.Value;
            oOrden.WOOIDCLIENTE     = (oOrder.customer_id == null) ? 0 : oOrder.customer_id.Value;
            oOrden.WOONOTACLIENTE   = oOrder.customer_note;

            if(oOrder.shipping_lines.Count > 0) {
                oOrden.WOOESDELIVERY = oOrder.shipping_lines[ 0 ].method_id != "local_pickup";
			} else
            {
                oOrden.WOOESDELIVERY = ((oOrder.shipping.address_1 + oOrder.shipping.address_2).Length > 0);
            }
            

            oOrden.oOrder = oOrder;
            oOrden.oWooOrderBilling  = ImportarOrderBilling( oOrder.billing );
            oOrden.oWooOrderShipping = ImportarOrderShipping( oOrder.shipping );

            oOrden.oDetallesDeLaOrden = ImportarDetallesDeLaOrden(oOrder);
            return oOrden;
        }
        
        private WooOrderBilling ImportarOrderBilling( WooCommerceNET.WooCommerce.v2.OrderBilling oOrderBilling ) {
            WooOrderBilling oDatosDeFacturacion = new WooOrderBilling();
            oDatosDeFacturacion.WOONOMBRE = oOrderBilling.first_name;
            oDatosDeFacturacion.WOOAPELLIDO = oOrderBilling.last_name;
            oDatosDeFacturacion.WOOCOMPANIA = oOrderBilling.company;
            oDatosDeFacturacion.WOODIRECCION = (oOrderBilling.address_1 + " " + oOrderBilling.address_2).Trim();
            oDatosDeFacturacion.WOOCODIGOPOSTAL = oOrderBilling.postcode;
            oDatosDeFacturacion.WOOEMAIL = oOrderBilling.email;
            oDatosDeFacturacion.WOOTELEFONO = oOrderBilling.phone;

            return oDatosDeFacturacion;
        }

        private WooOrderShipping ImportarOrderShipping( WooCommerceNET.WooCommerce.v2.OrderShipping oOrderShipping ) {
            WooOrderShipping oWooOrdenShipping = new WooOrderShipping();

            oWooOrdenShipping.WOONOMBRE = oOrderShipping.first_name;
            oWooOrdenShipping.WOOAPELLIDO = oOrderShipping.last_name;
            oWooOrdenShipping.WOOCOMPANIA = oOrderShipping.company;
            oWooOrdenShipping.WOODIRECCION = (oOrderShipping.address_1 + " " + oOrderShipping.address_2).Trim();
            oWooOrdenShipping.WOOCODIGOPOSTAL = oOrderShipping.postcode;

            return oWooOrdenShipping;
        }

        public BOResultadoWoo LeerOrdenPorID(int id)
        {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

			try {
                Order oOrder = (oWCObject.Order.Get( id )).Result;

                if (oOrder != null) {
                    oResultadoWoo.Objeto = ImportarOrden( oOrder );
                    oResultadoWoo.Exito = true;
                } else
                    oResultadoWoo.Exito = false;
            } catch ( Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }

            return oResultadoWoo;
        }

        public List<WooOrder> LeerTodasLasOrdenes() {
            // Parametros del Dictionary	{ "include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" }
            //  wcObject.Product.GetAll( new Dictionary<string, string>() { {"include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" } })
            //
            List<Order> orders = (oWCObject.Order.GetAll( new Dictionary<string, string>() )).Result;

            List<WooOrder> oOrdenes = new List<WooOrder>();
            if (orders != null)
                orders.ForEach( oOrder => oOrdenes.Add( ImportarOrden( oOrder ) ) );
            return oOrdenes;
        }

        public BOResultadoWoo LeerOrdenesPendientes() {
            //
            // Parametros del Dictionary	{ "include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" }
            //  wcObject.Product.GetAll( new Dictionary<string, string>() { {"include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" } })
            //

            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            try {
                List<Order> orders = oWCObject.Order.GetAll( new Dictionary<string, string>() { { "status", WooStatusOrder.PROCESSING }, { "per_page", "4" } } ).Result;

                List<WooOrder> oOrdenes = new List<WooOrder>();
                if (orders != null)
                    orders.ForEach( oOrder => oOrdenes.Add( ImportarOrden( oOrder ) ) );

                oResultadoWoo.Objeto = oOrdenes;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }

            return oResultadoWoo;
        }

        public BOResultadoWoo MarcarOrdenComoProcesada( WooOrder oOrden ) {
            BOResultadoWoo oResultadoWoo = new BOResultadoWoo();

            oOrden.oOrder.status = WooStatusOrder.COMPLETED;

            try {
                oWCObject.Order.UpdateWithNull( oOrden.oOrder.id.Value, new { status = WooStatusOrder.COMPLETED } );

                oResultadoWoo.Objeto = oOrden;
                oResultadoWoo.Exito = true;
            } catch (Exception e) {
                oResultadoWoo.Exito = false;
                oResultadoWoo.ListaErrores.Add( e.Message );
            }
            return oResultadoWoo;

        }
        #endregion

        #region DETALLE DE LA ORDEN
        public WooOrderLineItem ImportarDetalleOrden(WooCommerceNET.WooCommerce.v2.OrderLineItem oOrderLineItem)
        {
            WooOrderLineItem oDetalleOrden = new WooOrderLineItem();
            oDetalleOrden.WOOIDITEM = oOrderLineItem.id.Value;
            oDetalleOrden.WOONOMBREDELPRODUCTO = oOrderLineItem.name;
            oDetalleOrden.WOOCANTIDAD = oOrderLineItem.quantity.Value;
            oDetalleOrden.WOOSUBTOTAL = oOrderLineItem.subtotal.Value;
            oDetalleOrden.WOOIMPUESTOSUBTOTAL = oOrderLineItem.subtotal_tax.Value;
            oDetalleOrden.WOOTOTAL = oOrderLineItem.total.Value;
            oDetalleOrden.WOOPRECIO = oOrderLineItem.price.Value;
            oDetalleOrden.WOOIDPRODUCTO = oOrderLineItem.product_id.Value;

            foreach (WooCommerceNET.WooCommerce.v2.OrderMeta oOrderMeta in oOrderLineItem.meta_data) {
                if(oDetalleOrden.WOOTERMINOS.Length > 0 )
                    oDetalleOrden.WOOTERMINOS += ", ";
                
                oDetalleOrden.WOOTERMINOS += oOrderMeta.value; 
            }
            oDetalleOrden.oOrderLineItem = oOrderLineItem;

            return oDetalleOrden;
        }

        public List<WooOrderLineItem> ImportarDetallesDeLaOrden(Order order)
        {
            List<WooOrderLineItem> oDetallesDeLaOrden = new List<WooOrderLineItem>();

            if (order.line_items != null)
            {
                order.line_items.ForEach(orderLineItem => oDetallesDeLaOrden.Add(ImportarDetalleOrden(orderLineItem)));
            }
            return oDetallesDeLaOrden;
        }
        #endregion
    }
}
