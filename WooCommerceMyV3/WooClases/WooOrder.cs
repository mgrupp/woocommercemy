﻿using System;
using System.Collections.Generic;

using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy {
	public class WooOrder
	{
		public WooOrder() 
		{
			WOOIDORDEN		= 0;
			WOONUMEROORDEN	= "";
			WOOESTADO		= WooStatusOrder.PENDING;
			WOOMONEDA		= "";               
			WOOFECHADECREACION = DateTime.Now.Date;
			WOODESCUENTOTOTAL  = 0;
			WOODESCUENTOIMPUESTO = 0;
			WOOCOSTOENVIO	 = 0;
			WOOIMPUESTOENVIO = 0;
			WOOTOTAL		 = 0;
			WOOIMPUESTOTOTAL = 0;
			WOOIDCLIENTE	 = 0;
			WOONOTACLIENTE	 = "";
			WOOESDELIVERY    = false;

			oDetallesDeLaOrden = new List<WooOrderLineItem>();

			oOrder = null;
			oWooOrderBilling  = null;
			oWooOrderShipping = null;
		}

		public int WOOIDORDEN { get; set; }

		public string WOONUMEROORDEN { get; set; }

		public string WOOESTADO { get; set; }

		public string WOOMONEDA { get; set; }

		public DateTime WOOFECHADECREACION { get; set; }

		public decimal WOODESCUENTOTOTAL { get; set; }

		public decimal WOODESCUENTOIMPUESTO { get; set; }

		public decimal WOOCOSTOENVIO { get; set; }

		public decimal WOOIMPUESTOENVIO { get; set; }

		public decimal WOOTOTAL { get; set; }

		public decimal WOOIMPUESTOTOTAL { get; set; }

		public int WOOIDCLIENTE { get; set; }

		public string WOONOTACLIENTE { get; set; }

		public bool WOOESDELIVERY { get; set; }

		public Order oOrder { get; set; }

		public List<WooOrderLineItem> oDetallesDeLaOrden { get; set; }

		public WooOrderBilling oWooOrderBilling { get; set; }

		public  WooOrderShipping oWooOrderShipping{ get; set; }



	}
}
