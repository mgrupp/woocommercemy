﻿using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy {
	public class WooCustomerShipping {

		public string WOONOMBRE { get; set; }

		public string WOOAPELLIDO { get; set; }

		public string WOOCOMPANIA { get; set; }

		public string WOODIRECCION { get; set; }

		public string WOOCODIGOPOSTAL { get; set; }

		public CustomerBilling oCustomerBilling { get; set; }
	}
}
