﻿
namespace WooCommerceMy
{
	class WooCategoryDisplay {
		public static string DEFAULT = "default";

		public static string PRODUCTS = "products";

		public static string SUBCATEGORIES = "subcategories";

		public static string BOTH = "both";
	}
}
