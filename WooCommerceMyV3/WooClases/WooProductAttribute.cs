﻿using System;

using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy {
	public class WooProductAttribute {

		public WooProductAttribute() {
			WOONOMBREDELATRIBUTO = "";
		}

		public int WOOIDATRIBUTO {
			get {
				return oProductAttribute.id != null ? oProductAttribute.id.Value : 0;
			}
		}

		public string WOONOMBREDELATRIBUTO {
			get {
				return oProductAttribute.name;
			}
			set {
				oProductAttribute.name = value;
			}
		}

		public string WOOSLUG {
			get {
				return oProductAttribute.slug;
			}
			set {
				oProductAttribute.slug = value;
			}
		}

		public ProductAttribute oProductAttribute = new ProductAttribute();

		public override string ToString() {
			return WOONOMBREDELATRIBUTO;
		}

	}

}
