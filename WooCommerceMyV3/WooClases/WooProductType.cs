﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WooCommerceMy {
	public class WooProductType {

		public static string SIMPLE = "simple";

		public static string GROUPED = "grouped";

		public static string EXTERNAL = "external";

		public static string VARIABLE = "variable";

	}
}
