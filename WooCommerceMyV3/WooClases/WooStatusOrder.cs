﻿namespace WooCommerceMy {
	class WooStatusOrder {
		public static string PENDING= "pending";

		public static string PROCESSING = "processing";

		public static string ONHOLD = "on-hold";

		public static string COMPLETED = "completed";

		public static string CANCELLED = "cancelled";

		public static string REFUNDED = "refunded";

		public static string FAILED = "failed";

		public static string TRASH = "trash";
	}
}
