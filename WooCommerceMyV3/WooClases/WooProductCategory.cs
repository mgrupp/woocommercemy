﻿namespace WooCommerceMy {

	public class WooProductCategory
	{
		public WooProductCategory() 
		{
			WOOIDCATEGORIAPRODUCTO = 0;
			WOONOMBRECATEGORIA = "";
		}

		public int WOOIDCATEGORIAPRODUCTO { get; set; }

		public string WOONOMBRECATEGORIA { get; set; }
	}
}
