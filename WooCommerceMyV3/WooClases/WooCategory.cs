﻿
using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy
{
	public class WooCategory 
	{
		public WooCategory( ) 
		{
			WOOIDCATEGORIA		 = 0;
			WOONOMBRECATEGORIA	 = "";
			WOOIDCATEGORIAPADRE	 = 0;
			WOOMODOVISUALIZACION = WooCategoryDisplay.DEFAULT;

			oProductCategory = new ProductCategory();
		}

		public int WOOIDCATEGORIA { get; set; }

		public string WOONOMBRECATEGORIA { get; set; }

		public int WOOIDCATEGORIAPADRE { get; set; }

		public string WOOMODOVISUALIZACION = WooCategoryDisplay.DEFAULT;

		public ProductCategory oProductCategory { get; set; }

		public WooCategory oCategoriaPadre { get; set; }

		public override string ToString()
		{
			return WOOIDCATEGORIA + " " + WOONOMBRECATEGORIA;
		}


	};
}
