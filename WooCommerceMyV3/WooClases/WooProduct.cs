﻿using System;
using System.Collections.Generic;

using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy
{
    public class WooProduct
	{
		public WooProduct()
		{
			WOOIDPRODUCTO = 0;
			WOONOMBRELARGO = "";
			WOONOMBRECORTO = "";
			WOOTYPE = WooProductType.SIMPLE;
			WOOCARACTERISTICAS = "";
			WOOPRECIODEVENTA = 0;
			WOOURLIMAGEN = "";
			WOOPUBLICAR = false;
			WOOPRODUCTOENOFERTA = false;
			WOOINICIOOFERTA = DateTime.Now.Date;
			WOOFINALOFERTA = DateTime.Now.Date;
			WOOPRECIODEOFERTA = 0;
			WOOMANEJAEXISTENCIA = false;
			WOOEXISTENCIA = 0;
			WOOTOTALDEVENTAS = 0;

			oCategoriasProducto  = new List<WooProductCategory>();
			oVariaciones		 = new List<int>();
			oCategoriasProducto  = new List<WooProductCategory>();
			oWooProductAttributeLines = new List<WooProductAttributeLine>();

			oProduct = new Product();
			oProduct.categories = new List<ProductCategoryLine>();
			oProduct.attributes = new List<ProductAttributeLine>();
		}

		public int WOOIDPRODUCTO { get; set; }

		public string WOONOMBRELARGO { get; set; }

		public string WOONOMBRECORTO { get; set; }

		public string WOOTYPE { get; set; }

		public string WOOCARACTERISTICAS { get; set; }

		public decimal WOOPRECIODEVENTA { get; set; }

		public string WOOURLIMAGEN { get; set; }

		public bool WOOPUBLICAR { get; set; }

		public bool WOOPRODUCTOENOFERTA { get; set; }

		/// <summary>
		/// Fecha en que inicia la oferta
		/// </summary>
		public DateTime WOOINICIOOFERTA { get; set; }

		/// <summary>
		/// Fecha en la que culmina la oferta
		/// </summary>
		public DateTime WOOFINALOFERTA { get; set; } 

		public decimal WOOPRECIODEOFERTA { get; set; }

		public bool WOOMANEJAEXISTENCIA { get; set; }  

		public int WOOEXISTENCIA { get; set; }     

		public long WOOTOTALDEVENTAS { get; set; }

		public List<WooProductCategory> oCategoriasProducto { get; set; }

		public List<int> oVariaciones { get; set; }

		public List<WooProductAttributeLine> oWooProductAttributeLines { get; set; }

		public Product oProduct { get; set; }

		public ProductImage oProductImage { get; set; }

		public void AgregarCategoria(int Id ) {
			oCategoriasProducto.Add( new WooProductCategory() { WOOIDCATEGORIAPRODUCTO = Id });
		}

		public void EliminarTodasLasCategoria( ) {
			oCategoriasProducto.Clear();
		}

		public void EliminarTodosLosAtributos() {
			oWooProductAttributeLines.Clear();
		}
	}
}
