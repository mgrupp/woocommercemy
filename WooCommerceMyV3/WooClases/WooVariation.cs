﻿using System;
using System.Collections.Generic;

using WooCommerceNET.WooCommerce.v3;

using WooCommerceMy;

namespace WooCommerceMy {
	public class WooVariation {

		public WooVariation(){
			oVariation = new Variation();
		}

		public int WOOID 
		{
			get {
				return (oVariation.id != null) ? oVariation.id.Value : 0;
			}
			set { }
		}

		public string WOODESCRIPCION 
		{
			get {
				return oVariation.description;
			}
			set {
				oVariation.description = value;
			}
		}

		public decimal Precio {
			get {
				return (oVariation.regular_price != null) ? oVariation.regular_price.Value : 0;
			}
			set {
				oVariation.regular_price = value;
			}
		}

		public override string ToString() {
			return WOODESCRIPCION;
		}


		public Variation oVariation;
	}
}
