﻿
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooOrderLineItem
	{
		public WooOrderLineItem( ) {
			WOOIDITEM = 0;
			WOONOMBREDELPRODUCTO = "";
			WOOCANTIDAD = 0;
			WOOSUBTOTAL = 0;
			WOOIMPUESTOSUBTOTAL = 0;
			WOOTOTAL = 0;
			WOOPRECIO = 0;
			WOOIDPRODUCTO = 0;
			WOOTERMINOS = "";

			oOrderLineItem = null;
		}

		public int WOOIDITEM { get; set; }

		public string WOONOMBREDELPRODUCTO { get; set; }

		public decimal WOOCANTIDAD { get; set; }

		public decimal WOOSUBTOTAL { get; set; }

		public decimal WOOIMPUESTOSUBTOTAL { get; set; }

		public decimal WOOTOTAL { get; set; }

		public decimal WOOPRECIO { get; set; }

		public int WOOIDPRODUCTO { get; set; }

		public string WOOTERMINOS { get; set; }

		public OrderLineItem oOrderLineItem;

		

		


	}
}
