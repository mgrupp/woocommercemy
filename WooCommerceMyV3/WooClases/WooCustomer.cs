﻿
using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy {
	public class WooCustomer {

		public WooCustomer() {
			WOOIDCLIENTE	= 0;
			WOONOMBRE		= "";
			WOOAPELLIDO		= "";
			WOOEMAIL		= "";
			WOOUSUARIO		= "";
			WOOCLAVE		= "";

			oCustomer			 = null;
			oWooCustomerBilling  = new WooCustomerBilling();
			oWooCustomerShipping = new WooCustomerShipping();
		}

		public int WOOIDCLIENTE { get; set; }

		public string WOONOMBRE { get; set; }

		public string WOOAPELLIDO { get; set; }

		public string WOOEMAIL { get; set; }

		public string WOOUSUARIO { get; set; }

		public string WOOCLAVE { get; set; }

		public Customer oCustomer;

		public WooCustomerBilling oWooCustomerBilling;

		public WooCustomerShipping oWooCustomerShipping;
	}
}
