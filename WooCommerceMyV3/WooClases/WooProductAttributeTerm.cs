﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy {
	public class WooProductAttributeTerm {

		public WooProductAttributeTerm() {
			WOONOMBREDELTERMINO = "";
		}

		public int WOOIDPRODUCATTRIBUTETERM{
			get {
				return oProductAttributeTerm.id != null ? oProductAttributeTerm.id.Value : 0;
			}
		}

		public String WOONOMBREDELTERMINO {
			get {
				return oProductAttributeTerm.name != null ? oProductAttributeTerm.name : "";
			}
			set {
				oProductAttributeTerm.name = value;
			}
		}

		public String WOODESCRIPCIONDELTERMINO {
			get {
				return oProductAttributeTerm.description != null ? oProductAttributeTerm.description : "";
			}
			set {
				oProductAttributeTerm.description = value;
			}
		}

		public string WOOSLUG {
			get {
				return oProductAttributeTerm.slug != null ? oProductAttributeTerm.slug : "";
			}
			set {
				oProductAttributeTerm.slug = value;
			}
		}

		public ProductAttributeTerm oProductAttributeTerm = new ProductAttributeTerm();

		public override string ToString() {
			return WOONOMBREDELTERMINO;
		}
	}


}
