﻿
using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy {
	public class WooCustomerBilling {

		public WooCustomerBilling() {
			WOONOMBRE		= "";
			WOOAPELLIDO		= "";
			WOOCOMPANIA		= "";
			WOODIRECCION	= "";
			WOOCODIGOPOSTAL	= "";
			WOOEMAIL		= "";
			WOOTELEFONO		= "";
		}

		public string WOONOMBRE { get; set; }

		public string WOOAPELLIDO { get; set; }

		public string WOOCOMPANIA { get; set; }

		public string WOODIRECCION { get; set; }

		public string WOOCODIGOPOSTAL { get; set; }

		public string WOOEMAIL { get; set; }

		public string WOOTELEFONO { get; set; }

		public CustomerBilling oCustomerBilling { get; set; }
	}
}
