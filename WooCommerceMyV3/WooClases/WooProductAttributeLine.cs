﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v3;

namespace WooCommerceMy {
	public class WooProductAttributeLine {

		public WooProductAttributeLine() {
			oProductAttributeLine			= new ProductAttributeLine();
			oProductAttributeLine.variation = true;
			oProductAttributeLine.visible	= true;
			oProductAttributeLine.options	= new List<string>();
		}

		public int WOOIDPRODUCTATTRIBUTELINE {
			get {
				return (oProductAttributeLine.id != null) ? oProductAttributeLine.id.Value : 0;
			}
			set {
				oProductAttributeLine.id = value;
			}
		}

		public string WOONOMBREATRIBUTO {
			get {
				return oProductAttributeLine.name;
			}
			set {
				oProductAttributeLine.name = value;
			}
		}

		public bool SeUsaComoVariacion {
			get {
				return (oProductAttributeLine.variation != null) ? oProductAttributeLine.variation.Value : false ;
			}
			set {
				oProductAttributeLine.variation = value;
			}
		}

		public List<string> Terminos {
			get {
				return oProductAttributeLine.options;
			}
		}

		public ProductAttributeLine oProductAttributeLine;
	}
}
