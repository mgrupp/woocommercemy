﻿using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooOrderShipping {

		public WooOrderShipping() {
			WOONOMBRE		= "";
			WOOAPELLIDO		= "";
			WOOCOMPANIA		= "";
			WOODIRECCION	= "";
			WOOCODIGOPOSTAL = "";
		}

		public string WOONOMBRE { get; set; }

		public string WOOAPELLIDO { get; set; }

		public string WOOCOMPANIA { get; set; }

		public string WOODIRECCION { get; set; }

		public string WOOCODIGOPOSTAL { get; set; }

		public OrderShipping oOrderShipping { get; set; }
	}
}
