﻿using System.Collections.Generic;

namespace WooCommerceMy {
	public class WooError {

		public string code { get; set; }

		public string message { get; set; }

		public int dataStatus {
			get {
				int resultado = 0;
				data.TryGetValue( "status", out resultado );
				return resultado;
			}
			set { } }

		public Dictionary<string, int> data;
	}

	public enum WooErrorStatus : int {
		none = 0,
		NoSePudoCrear = 400
	}
}
